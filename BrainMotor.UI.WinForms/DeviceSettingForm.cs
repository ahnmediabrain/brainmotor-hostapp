﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using BocpDriverDotNet;

namespace BrainMotor.UI.WinForms
{
    public partial class DeviceSettingForm : Form
    {
        //BocpDeviceSettings _originalSettings;
        //BocpHostDotNet _bocpHost;

        public DeviceSettingForm(BocpHostDotNet bocpHost)
        {
            //_bocpHost = bocpHost;
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //BocpDeviceSettings newSettings = GatherDataFromUI();
            //if (newSettings == null)
            //    return;

            //_bocpHost.UpdateSettingsAsync(newSettings).Wait(); // 변경된 부분만 알아서 갱신하도록 구현할 것.
        }

        //private void ApplyDataToUI(BocpDeviceSettings settings)
        //{
        //    if (settings.Direction == BocpMotorDirection::DirectionA)
        //    {
        //        this.radioBtnDirectionA.Checked = true;
        //    }
        //    else
        //    {
        //        this.radioBtnDirectionB.Checked = true;
        //    }

        //    switch (settings.MotionType)
        //    {
        //        case BocpMotionType::Rotational:
        //            this.radioBtnRotationalMotion.Checked = true;
        //            break;
        //        case BocpMotionType::Linear:
        //            this.radioBtnLinearMotion.Checked = true;
        //            break;
        //    }

        //    this.checkBoxHoldMotorWhenStopped = settings.HoldMotor;
        //    this.tbPulsePerRev.Text = settings.PulsePerRev.ToString();
        //    this.tbDistancePerRev.Text = settings.DistancePerRev.ToString();
        //}
        //private BocpDeviceSettings GatherDataFromUI()
        //{
        //    BocpDeviceSettings settings = new BocpDeviceSettings();
        //    settings.Direction = (this.radioBtnDirectionA.Checked) ? BocpMotorDirection::DirectionA : BocpMotorDirection::DirectionB;
        //    settings.MotionType = (this.radioBtnRotationalMotion.Checked) ? BocpMotionType::Rotational : BocpMotionType::Linear;
        //    settings.HoldMotor = this.checkBoxHoldMotorWhenStopped;

        //    int pulsePerRev;
        //    if (!int.TryParse(this.tbPulsePerRev.Text, out pulsePerRev))
        //    {
        //        MessageBox.Show("올바른 pulse per rev 입력!");
        //        return null;
        //    }
        //    settings.PulsePerRev = pulsePerRev;

        //    int distancePerRev;
        //    if (!int.TryParse(this.tbDistancePerRev.Text, out distancePerRev))
        //    {
        //        MessageBox.Show("올바른 distance per rev 입력!");
        //        return null;
        //    }
        //    settings.DistancePerRev = distancePerRev;

        //    return settings;
        //}

        private void DeviceSettingForm_Load(object sender, EventArgs e)
        {
            //Task<BocpOperationResult> task = _bocpHost.QuerySettingsAsync();
            //task.Wait();
            //BocpOperationResult result = task.Result;

            //if (result.Status != BocpStatusCode.Success)
            //{
            //    MessageBox.Show("디바이스에 설정을 쿼리할 수 없음");
            //    return;
            //}

            //_originalSettings = result.QueryResult as BocpDeviceSettings;
            //ApplyDataToUI(_originalSettings);
        }
    }
}
