﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using BocpDriverDotNet;

namespace BrainMotor.UI.WinForms
{
    public partial class DeviceControlForm : Form
    {
        string _serialPort;
        BocpHostDotNet _bocpHost;

        public DeviceControlForm(string serialPort)
        {
            _serialPort = serialPort;
            InitializeComponent();
        }

        private async void btnCVMDrive_Click(object sender, EventArgs e)
        {
            double micrometersPerSecond;
            if (!double.TryParse(this.tbVelocity.Text, out micrometersPerSecond))
            {
                MessageBox.Show("유효한 속도를 입력!");
                return;
            }

            BocpOperationResult result = await _bocpHost.StartLinearConstantVelocityModeAsync(micrometersPerSecond);
            WriteLog("등속도 구동 명령 수행 결과: " + result.Status.ToString());
        }

        private async void btnStopMotion_Click(object sender, EventArgs e)
        {
            BocpOperationResult result = await _bocpHost.StopOscillationAsync();
            WriteLog("구동 정지 명령 수행 결과: " + result.Status.ToString());
        }

        private async void btnSinusoidalDrive_Click(object sender, EventArgs e)
        {
            double amplitudeInMillimeters;
            double frequencyHz;
            double phaseConstantInRadian;

            if (!double.TryParse(this.tbSinusodialDrive_AmplitudeInMillimeters.Text, out amplitudeInMillimeters))
            {
                MessageBox.Show("유효한 진폭을 입력!");
                return;
            }
            if (!double.TryParse(this.tbSinusoidalDrive_FrequencyHz.Text, out frequencyHz))
            {
                MessageBox.Show("유효한 진동수를 입력!");
                return;
            }
            if (!double.TryParse(this.tbSinusoidalDrive_PhaseConstantInRadian.Text, out phaseConstantInRadian))
            {
                MessageBox.Show("유효한 위상상수를 입력!");
                return;
            }

            double amplitudeInMicrometers = amplitudeInMillimeters * 1000.0;
            double angularFrequency = frequencyHz * 2 * Math.PI;

            BocpOperationResult result = await _bocpHost.StartLinearSinusoidalDriveAsync(amplitudeInMicrometers, angularFrequency, phaseConstantInRadian);
            WriteLog("사인형 구동 명령 수행 결과: " + result.Status.ToString());
        }

        private async void btnCVMDrive_Rotation_Click(object sender, EventArgs e)
        {
            double radiansPerSecond;
            if (!double.TryParse(this.tbAngularVelocityInRadian.Text, out radiansPerSecond))
            {
                MessageBox.Show("유효한 속도를 입력!");
                return;
            }

            double microradiansPerSecond = radiansPerSecond * Math.Pow(10, 6);
            BocpOperationResult result = await _bocpHost.StartRotationalConstantVelocityModeAsync(radiansPerSecond * Math.Pow(10, 6));
            WriteLog("등속도 구동 명령 수행 결과: " + result.Status.ToString());
        }

        private async void btnSinusoidalDrive_Rotation_Click(object sender, EventArgs e)
        {
            double amplitudeInDegrees;
            double frequencyHz;
            double phaseConstantInRadian;

            if (!double.TryParse(this.tbSinusoidalDrive_AngularAmp_Degrees_Rotation.Text, out amplitudeInDegrees))
            {
                MessageBox.Show("유효한 진폭을 입력!");
                return;
            }
            if (!double.TryParse(this.tbFrequency_Rotation.Text, out frequencyHz))
            {
                MessageBox.Show("유효한 진동수를 입력!");
                return;
            }
            if (!double.TryParse(this.tbPhaseConstant_Rad_Rotation.Text, out phaseConstantInRadian))
            {
                MessageBox.Show("유효한 위상상수를 입력!");
                return;
            }

            double amplitudeInMicroradians = amplitudeInDegrees * (2 * Math.PI / 360.0) * Math.Pow(10, 6);
            double angularFrequency = frequencyHz * 2 * Math.PI;

            BocpOperationResult result = await _bocpHost.StartRotationalSinusoidalDriveAsync(amplitudeInMicroradians, angularFrequency, phaseConstantInRadian);
            WriteLog("사인형 구동 명령 수행 결과: " + result.Status.ToString());
        }

        private async void btnFineControl_Click(object sender, EventArgs e)
        {
            int steps;
            if (!int.TryParse(this.tbSteps.Text, out steps))
            {
                MessageBox.Show("유효한 스텝 수(정수)를 입력!");
                return;
            }
            await FineControl(steps);
        }

        private void DeviceSetting_Click(object sender, EventArgs e)
        {
            DeviceSettingForm deviceSettingForm = new DeviceSettingForm(_bocpHost);
            deviceSettingForm.Show(this);
        }

        private async void btnPowerOnMotor_Click(object sender, EventArgs e)
        {
            BocpOperationResult result = await _bocpHost.ControlMotorPowerAsync(true);
            WriteLog("전원 켜기 수행 결과: " + result.Status.ToString());
        }

        private async void btnPowerOffMotor_Click(object sender, EventArgs e)
        {
            BocpOperationResult result = await _bocpHost.ControlMotorPowerAsync(false);
            WriteLog("전원 끄기 수행 결과: " + result.Status.ToString());
        }

        private void DeviceControlForm_Load(object sender, EventArgs e)
        {
            _bocpHost = new BocpHostDotNet();
            Task<BocpOperationResult> task = _bocpHost.ConnectAsync(_serialPort);
            task.Wait();

            if (task.Result.Status != BocpStatusCode.Success)
            {
                MessageBox.Show("연결 실패\n 오류: " + task.Result.Status.ToString());
                Close();
            }

            this.Text = string.Format("BrainMotor - Device Control: Connected to {0}", _serialPort);
        }
        private void DeviceControlForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            _bocpHost.DisconnectAsync().Wait();
            _bocpHost.Dispose();
        }

        private async void btnLeft100Steps_Click(object sender, EventArgs e)
        {
            await FineControl(-100);
        }

        private async void btnLeft10Steps_Click(object sender, EventArgs e)
        {
            await FineControl(-10);
        }

        private async void btnLeft1Step_Click(object sender, EventArgs e)
        {
            await FineControl(-1);
        }

        private async void btnRight1Step_Click(object sender, EventArgs e)
        {
            await FineControl(1);
        }

        private async void btnRight10Steps_Click(object sender, EventArgs e)
        {
            await FineControl(10);
        }

        private async void btnRight100Steps_Click(object sender, EventArgs e)
        {
            await FineControl(100);
        }

        private async Task FineControl(int steps)
        {
            BocpOperationResult result = await _bocpHost.FineControlAsync(steps);
            WriteLog("미세 조정 수행 결과: " + result.Status.ToString());
        }

        private void WriteLog(string messageBody)
        {
            DateTime now = DateTime.Now;
            string messageFull = string.Format("[{0:yyyy/MM/dd HH:mm:ss}.{1:D3}] {2}", now, now.Millisecond, messageBody);
            this.listBoxLog.Items.Add(messageFull);
            this.listBoxLog.SelectedIndex = this.listBoxLog.Items.Count - 1;
        }
    }
}
