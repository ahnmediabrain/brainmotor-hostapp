﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using BocpDriverDotNet;

namespace BrainMotor.UI.WinForms
{
    public partial class MainForm : Form
    {
        //BocpHostDotNet _bocpHost_COM6;
        //BocpHostDotNet _bocpHost_COM7;

        public MainForm()
        {
            InitializeComponent();
        }

        private void ConnectToDevice_Click(object sender, EventArgs e)
        {
            PortSelectionForm portSelectionForm = new PortSelectionForm();
            if (portSelectionForm.ShowDialog(this) != DialogResult.OK ||
                portSelectionForm.SelectedPort == null)
                return;

            COMPortInfo selectedPort = portSelectionForm.SelectedPort;
            DeviceControlForm deviceControlForm = new DeviceControlForm(selectedPort.Name);
            deviceControlForm.Show();
        }

        //private async void button2_Click(object sender, EventArgs e)
        //{
        //    BocpHostDotNet _bocpHost_COM6 = new BocpHostDotNet();
        //    await _bocpHost_COM6.ConnectAsync("\\\\.\\COM6");
        //    BocpHostDotNet _bocpHost_COM7 = new BocpHostDotNet();
        //    await _bocpHost_COM7.ConnectAsync("\\\\.\\COM7");

        //    await _bocpHost_COM6.ControlMotorPowerAsync(true);
        //    await _bocpHost_COM7.ControlMotorPowerAsync(true);

        //    double p;
        //    if (!double.TryParse(this.tbPhaseDifference.Text, out p))
        //    {
        //        MessageBox.Show("올바른 위상 입력!");
        //        return;
        //    }

        //    await _bocpHost_COM6.StartLinearSinusoidalDriveAsync(3000, 0.5 * 2 * Math.PI, p);
        //    await _bocpHost_COM7.StartRotationalSinusoidalDriveAsync(10 * (Math.PI / 180) * Math.Pow(10, 6), 0.5 * 2 * Math.PI, 0);

        //    await _bocpHost_COM6.DisconnectAsync();
        //    await _bocpHost_COM7.DisconnectAsync();
        //}
    }
}
