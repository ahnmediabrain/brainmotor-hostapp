﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BrainMotor.UI.WinForms
{
    public partial class PortSelectionForm : Form
    {
        public COMPortInfo SelectedPort { get; set; }

        public PortSelectionForm()
        {
            InitializeComponent();

            this.SelectedPort = null;

            this.availableCOMPortList.DataSource = COMPortInfo.GetCOMPortsInfo();
            this.availableCOMPortList.DisplayMember = "Description";
            this.availableCOMPortList.ValueMember = "Description";
            this.availableCOMPortList.SelectedIndex = -1;
            this.okButton.Enabled = false;
        }

        private void COMPortList_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool bEnableOK = this.availableCOMPortList.SelectedIndex != -1;
            this.okButton.Enabled = bEnableOK;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            this.SelectedPort = (COMPortInfo)this.availableCOMPortList.SelectedItem;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            this.SelectedPort = null;
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }
    }
}
