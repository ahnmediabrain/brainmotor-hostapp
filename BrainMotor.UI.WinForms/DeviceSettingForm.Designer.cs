﻿namespace BrainMotor.UI.WinForms
{
    partial class DeviceSettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.checkBoxHoldMotorWhenStopped = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbDistancePerRev = new System.Windows.Forms.TextBox();
            this.tbPulsePerRev = new System.Windows.Forms.TextBox();
            this.radioBtnLinearMotion = new System.Windows.Forms.RadioButton();
            this.radioBtnRotationalMotion = new System.Windows.Forms.RadioButton();
            this.radioBtnDirectionB = new System.Windows.Forms.RadioButton();
            this.radioBtnDirectionA = new System.Windows.Forms.RadioButton();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.checkBoxHoldMotorWhenStopped);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.tbDistancePerRev);
            this.groupBox3.Controls.Add(this.tbPulsePerRev);
            this.groupBox3.Controls.Add(this.radioBtnLinearMotion);
            this.groupBox3.Controls.Add(this.radioBtnRotationalMotion);
            this.groupBox3.Controls.Add(this.radioBtnDirectionB);
            this.groupBox3.Controls.Add(this.radioBtnDirectionA);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(295, 203);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "장치 설정";
            // 
            // checkBoxHoldMotorWhenStopped
            // 
            this.checkBoxHoldMotorWhenStopped.AutoSize = true;
            this.checkBoxHoldMotorWhenStopped.Location = new System.Drawing.Point(138, 88);
            this.checkBoxHoldMotorWhenStopped.Name = "checkBoxHoldMotorWhenStopped";
            this.checkBoxHoldMotorWhenStopped.Size = new System.Drawing.Size(15, 14);
            this.checkBoxHoldMotorWhenStopped.TabIndex = 16;
            this.checkBoxHoldMotorWhenStopped.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 24);
            this.label1.TabIndex = 22;
            this.label1.Text = "구동하지 않을 때\r\n모터 홀딩";
            // 
            // tbDistancePerRev
            // 
            this.tbDistancePerRev.Location = new System.Drawing.Point(138, 148);
            this.tbDistancePerRev.Name = "tbDistancePerRev";
            this.tbDistancePerRev.Size = new System.Drawing.Size(137, 21);
            this.tbDistancePerRev.TabIndex = 17;
            // 
            // tbPulsePerRev
            // 
            this.tbPulsePerRev.Location = new System.Drawing.Point(138, 116);
            this.tbPulsePerRev.Name = "tbPulsePerRev";
            this.tbPulsePerRev.Size = new System.Drawing.Size(137, 21);
            this.tbPulsePerRev.TabIndex = 16;
            // 
            // radioBtnLinearMotion
            // 
            this.radioBtnLinearMotion.AutoSize = true;
            this.radioBtnLinearMotion.Location = new System.Drawing.Point(211, 59);
            this.radioBtnLinearMotion.Name = "radioBtnLinearMotion";
            this.radioBtnLinearMotion.Size = new System.Drawing.Size(47, 16);
            this.radioBtnLinearMotion.TabIndex = 15;
            this.radioBtnLinearMotion.TabStop = true;
            this.radioBtnLinearMotion.Text = "선형";
            this.radioBtnLinearMotion.UseVisualStyleBackColor = true;
            // 
            // radioBtnRotationalMotion
            // 
            this.radioBtnRotationalMotion.AutoSize = true;
            this.radioBtnRotationalMotion.Location = new System.Drawing.Point(138, 59);
            this.radioBtnRotationalMotion.Name = "radioBtnRotationalMotion";
            this.radioBtnRotationalMotion.Size = new System.Drawing.Size(47, 16);
            this.radioBtnRotationalMotion.TabIndex = 14;
            this.radioBtnRotationalMotion.TabStop = true;
            this.radioBtnRotationalMotion.Text = "회전";
            this.radioBtnRotationalMotion.UseVisualStyleBackColor = true;
            // 
            // radioBtnDirectionB
            // 
            this.radioBtnDirectionB.AutoSize = true;
            this.radioBtnDirectionB.Location = new System.Drawing.Point(211, 29);
            this.radioBtnDirectionB.Name = "radioBtnDirectionB";
            this.radioBtnDirectionB.Size = new System.Drawing.Size(55, 16);
            this.radioBtnDirectionB.TabIndex = 13;
            this.radioBtnDirectionB.TabStop = true;
            this.radioBtnDirectionB.Text = "방향B";
            this.radioBtnDirectionB.UseVisualStyleBackColor = true;
            // 
            // radioBtnDirectionA
            // 
            this.radioBtnDirectionA.AutoSize = true;
            this.radioBtnDirectionA.Location = new System.Drawing.Point(138, 29);
            this.radioBtnDirectionA.Name = "radioBtnDirectionA";
            this.radioBtnDirectionA.Size = new System.Drawing.Size(55, 16);
            this.radioBtnDirectionA.TabIndex = 12;
            this.radioBtnDirectionA.TabStop = true;
            this.radioBtnDirectionA.Text = "방향A";
            this.radioBtnDirectionA.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 60);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 12);
            this.label8.TabIndex = 11;
            this.label8.Text = "운동 종류";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 145);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 24);
            this.label7.TabIndex = 10;
            this.label7.Text = "회전당 이동거리\r\n(선형진동자의 경우)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 12);
            this.label6.TabIndex = 9;
            this.label6.Text = "회전당 펄스 수";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 12);
            this.label5.TabIndex = 8;
            this.label5.Text = "(+) 방향";
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(12, 221);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(295, 34);
            this.btnClose.TabIndex = 15;
            this.btnClose.Text = "완료";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // DeviceSettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 266);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.groupBox3);
            this.Name = "DeviceSettingForm";
            this.Text = "DeviceSettingForm";
            this.Load += new System.EventHandler(this.DeviceSettingForm_Load);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbDistancePerRev;
        private System.Windows.Forms.TextBox tbPulsePerRev;
        private System.Windows.Forms.RadioButton radioBtnLinearMotion;
        private System.Windows.Forms.RadioButton radioBtnRotationalMotion;
        private System.Windows.Forms.RadioButton radioBtnDirectionB;
        private System.Windows.Forms.RadioButton radioBtnDirectionA;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.CheckBox checkBoxHoldMotorWhenStopped;
        private System.Windows.Forms.Label label1;
    }
}