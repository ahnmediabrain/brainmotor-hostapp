﻿namespace BrainMotor.UI.WinForms
{
    partial class DeviceControlForm
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbVelocity = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnCVMDrive = new System.Windows.Forms.Button();
            this.btnStopMotion = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnSinusoidalDrive = new System.Windows.Forms.Button();
            this.tbSinusodialDrive_AmplitudeInMillimeters = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tbSinusoidalDrive_FrequencyHz = new System.Windows.Forms.TextBox();
            this.tbSinusoidalDrive_PhaseConstantInRadian = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDeviceSetting = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tbPhaseConstant_Rad_Rotation = new System.Windows.Forms.TextBox();
            this.tbFrequency_Rotation = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbSinusoidalDrive_AngularAmp_Degrees_Rotation = new System.Windows.Forms.TextBox();
            this.btnSinusoidalDrive_Rotation = new System.Windows.Forms.Button();
            this.btnCVMDrive_Rotation = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tbAngularVelocityInRadian = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tbSteps = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnFineControl = new System.Windows.Forms.Button();
            this.btnPowerOnMotor = new System.Windows.Forms.Button();
            this.btnPowerOffMotor = new System.Windows.Forms.Button();
            this.btnLeft100Steps = new System.Windows.Forms.Button();
            this.btnLeft10Steps = new System.Windows.Forms.Button();
            this.btnLeft1Step = new System.Windows.Forms.Button();
            this.btnRight1Step = new System.Windows.Forms.Button();
            this.btnRight10Steps = new System.Windows.Forms.Button();
            this.btnRight100Steps = new System.Windows.Forms.Button();
            this.listBoxLog = new System.Windows.Forms.ListBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbVelocity
            // 
            this.tbVelocity.Location = new System.Drawing.Point(44, 137);
            this.tbVelocity.Name = "tbVelocity";
            this.tbVelocity.Size = new System.Drawing.Size(91, 21);
            this.tbVelocity.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(42, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "속도 (um/s)";
            // 
            // btnCVMDrive
            // 
            this.btnCVMDrive.Location = new System.Drawing.Point(164, 135);
            this.btnCVMDrive.Name = "btnCVMDrive";
            this.btnCVMDrive.Size = new System.Drawing.Size(138, 22);
            this.btnCVMDrive.TabIndex = 2;
            this.btnCVMDrive.Text = "등속도 구동 명령 전송";
            this.btnCVMDrive.UseVisualStyleBackColor = true;
            this.btnCVMDrive.Click += new System.EventHandler(this.btnCVMDrive_Click);
            // 
            // btnStopMotion
            // 
            this.btnStopMotion.Location = new System.Drawing.Point(139, 12);
            this.btnStopMotion.Name = "btnStopMotion";
            this.btnStopMotion.Size = new System.Drawing.Size(111, 60);
            this.btnStopMotion.TabIndex = 3;
            this.btnStopMotion.Text = "구동 정지";
            this.btnStopMotion.UseVisualStyleBackColor = true;
            this.btnStopMotion.Click += new System.EventHandler(this.btnStopMotion_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(28, 104);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(285, 68);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "등속도 제어";
            // 
            // btnSinusoidalDrive
            // 
            this.btnSinusoidalDrive.Location = new System.Drawing.Point(61, 104);
            this.btnSinusoidalDrive.Name = "btnSinusoidalDrive";
            this.btnSinusoidalDrive.Size = new System.Drawing.Size(156, 23);
            this.btnSinusoidalDrive.TabIndex = 5;
            this.btnSinusoidalDrive.Text = "사인형 구동 명령 전송";
            this.btnSinusoidalDrive.UseVisualStyleBackColor = true;
            this.btnSinusoidalDrive.Click += new System.EventHandler(this.btnSinusoidalDrive_Click);
            // 
            // tbSinusodialDrive_AmplitudeInMillimeters
            // 
            this.tbSinusodialDrive_AmplitudeInMillimeters.Location = new System.Drawing.Point(108, 22);
            this.tbSinusodialDrive_AmplitudeInMillimeters.Name = "tbSinusodialDrive_AmplitudeInMillimeters";
            this.tbSinusodialDrive_AmplitudeInMillimeters.Size = new System.Drawing.Size(166, 21);
            this.tbSinusodialDrive_AmplitudeInMillimeters.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(32, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "진폭 (mm)";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 53);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "진동수 (Hz)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "위상상수 (rad)";
            // 
            // tbSinusoidalDrive_FrequencyHz
            // 
            this.tbSinusoidalDrive_FrequencyHz.Location = new System.Drawing.Point(108, 47);
            this.tbSinusoidalDrive_FrequencyHz.Name = "tbSinusoidalDrive_FrequencyHz";
            this.tbSinusoidalDrive_FrequencyHz.Size = new System.Drawing.Size(166, 21);
            this.tbSinusoidalDrive_FrequencyHz.TabIndex = 10;
            // 
            // tbSinusoidalDrive_PhaseConstantInRadian
            // 
            this.tbSinusoidalDrive_PhaseConstantInRadian.Location = new System.Drawing.Point(108, 73);
            this.tbSinusoidalDrive_PhaseConstantInRadian.Name = "tbSinusoidalDrive_PhaseConstantInRadian";
            this.tbSinusoidalDrive_PhaseConstantInRadian.Size = new System.Drawing.Size(166, 21);
            this.tbSinusoidalDrive_PhaseConstantInRadian.TabIndex = 11;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tbSinusoidalDrive_PhaseConstantInRadian);
            this.groupBox2.Controls.Add(this.tbSinusoidalDrive_FrequencyHz);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.tbSinusodialDrive_AmplitudeInMillimeters);
            this.groupBox2.Controls.Add(this.btnSinusoidalDrive);
            this.groupBox2.Location = new System.Drawing.Point(28, 188);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(285, 139);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "사인형 진동 제어";
            // 
            // btnDeviceSetting
            // 
            this.btnDeviceSetting.Location = new System.Drawing.Point(12, 12);
            this.btnDeviceSetting.Name = "btnDeviceSetting";
            this.btnDeviceSetting.Size = new System.Drawing.Size(111, 60);
            this.btnDeviceSetting.TabIndex = 14;
            this.btnDeviceSetting.Text = "장치 설정";
            this.btnDeviceSetting.UseVisualStyleBackColor = true;
            this.btnDeviceSetting.Click += new System.EventHandler(this.DeviceSetting_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Location = new System.Drawing.Point(12, 84);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(317, 257);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "선형 제어";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tbPhaseConstant_Rad_Rotation);
            this.groupBox4.Controls.Add(this.tbFrequency_Rotation);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.tbSinusoidalDrive_AngularAmp_Degrees_Rotation);
            this.groupBox4.Controls.Add(this.btnSinusoidalDrive_Rotation);
            this.groupBox4.Location = new System.Drawing.Point(351, 188);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(285, 139);
            this.groupBox4.TabIndex = 20;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "사인형 진동 제어";
            // 
            // tbPhaseConstant_Rad_Rotation
            // 
            this.tbPhaseConstant_Rad_Rotation.Location = new System.Drawing.Point(108, 73);
            this.tbPhaseConstant_Rad_Rotation.Name = "tbPhaseConstant_Rad_Rotation";
            this.tbPhaseConstant_Rad_Rotation.Size = new System.Drawing.Size(166, 21);
            this.tbPhaseConstant_Rad_Rotation.TabIndex = 11;
            // 
            // tbFrequency_Rotation
            // 
            this.tbFrequency_Rotation.Location = new System.Drawing.Point(108, 47);
            this.tbFrequency_Rotation.Name = "tbFrequency_Rotation";
            this.tbFrequency_Rotation.Size = new System.Drawing.Size(166, 21);
            this.tbFrequency_Rotation.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "위상상수 (rad)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(27, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "진동수 (Hz)";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(40, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 12);
            this.label7.TabIndex = 7;
            this.label7.Text = "진폭 (도)";
            // 
            // tbSinusoidalDrive_AngularAmp_Degrees_Rotation
            // 
            this.tbSinusoidalDrive_AngularAmp_Degrees_Rotation.Location = new System.Drawing.Point(108, 22);
            this.tbSinusoidalDrive_AngularAmp_Degrees_Rotation.Name = "tbSinusoidalDrive_AngularAmp_Degrees_Rotation";
            this.tbSinusoidalDrive_AngularAmp_Degrees_Rotation.Size = new System.Drawing.Size(166, 21);
            this.tbSinusoidalDrive_AngularAmp_Degrees_Rotation.TabIndex = 6;
            // 
            // btnSinusoidalDrive_Rotation
            // 
            this.btnSinusoidalDrive_Rotation.Location = new System.Drawing.Point(61, 104);
            this.btnSinusoidalDrive_Rotation.Name = "btnSinusoidalDrive_Rotation";
            this.btnSinusoidalDrive_Rotation.Size = new System.Drawing.Size(156, 23);
            this.btnSinusoidalDrive_Rotation.TabIndex = 5;
            this.btnSinusoidalDrive_Rotation.Text = "사인형 구동 명령 전송";
            this.btnSinusoidalDrive_Rotation.UseVisualStyleBackColor = true;
            this.btnSinusoidalDrive_Rotation.Click += new System.EventHandler(this.btnSinusoidalDrive_Rotation_Click);
            // 
            // btnCVMDrive_Rotation
            // 
            this.btnCVMDrive_Rotation.Location = new System.Drawing.Point(487, 135);
            this.btnCVMDrive_Rotation.Name = "btnCVMDrive_Rotation";
            this.btnCVMDrive_Rotation.Size = new System.Drawing.Size(138, 22);
            this.btnCVMDrive_Rotation.TabIndex = 18;
            this.btnCVMDrive_Rotation.Text = "등속도 구동 명령 전송";
            this.btnCVMDrive_Rotation.UseVisualStyleBackColor = true;
            this.btnCVMDrive_Rotation.Click += new System.EventHandler(this.btnCVMDrive_Rotation_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(365, 122);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 12);
            this.label8.TabIndex = 17;
            this.label8.Text = "각속도 (rad/s)";
            // 
            // tbAngularVelocityInRadian
            // 
            this.tbAngularVelocityInRadian.Location = new System.Drawing.Point(367, 137);
            this.tbAngularVelocityInRadian.Name = "tbAngularVelocityInRadian";
            this.tbAngularVelocityInRadian.Size = new System.Drawing.Size(91, 21);
            this.tbAngularVelocityInRadian.TabIndex = 16;
            // 
            // groupBox5
            // 
            this.groupBox5.Location = new System.Drawing.Point(351, 104);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(285, 68);
            this.groupBox5.TabIndex = 19;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "등속도 제어";
            // 
            // groupBox6
            // 
            this.groupBox6.Location = new System.Drawing.Point(335, 84);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(317, 257);
            this.groupBox6.TabIndex = 21;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "회전 운동 제어";
            // 
            // tbSteps
            // 
            this.tbSteps.Location = new System.Drawing.Point(23, 397);
            this.tbSteps.Name = "tbSteps";
            this.tbSteps.Size = new System.Drawing.Size(137, 21);
            this.tbSteps.TabIndex = 22;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(21, 377);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(101, 12);
            this.label9.TabIndex = 12;
            this.label9.Text = "미세 조정 스텝 수";
            // 
            // btnFineControl
            // 
            this.btnFineControl.Location = new System.Drawing.Point(176, 389);
            this.btnFineControl.Name = "btnFineControl";
            this.btnFineControl.Size = new System.Drawing.Size(176, 35);
            this.btnFineControl.TabIndex = 23;
            this.btnFineControl.Text = "미세 조정 명령 전송";
            this.btnFineControl.UseVisualStyleBackColor = true;
            this.btnFineControl.Click += new System.EventHandler(this.btnFineControl_Click);
            // 
            // btnPowerOnMotor
            // 
            this.btnPowerOnMotor.Location = new System.Drawing.Point(267, 12);
            this.btnPowerOnMotor.Name = "btnPowerOnMotor";
            this.btnPowerOnMotor.Size = new System.Drawing.Size(111, 60);
            this.btnPowerOnMotor.TabIndex = 24;
            this.btnPowerOnMotor.Text = "모터 전원 켜기";
            this.btnPowerOnMotor.UseVisualStyleBackColor = true;
            this.btnPowerOnMotor.Click += new System.EventHandler(this.btnPowerOnMotor_Click);
            // 
            // btnPowerOffMotor
            // 
            this.btnPowerOffMotor.Location = new System.Drawing.Point(393, 12);
            this.btnPowerOffMotor.Name = "btnPowerOffMotor";
            this.btnPowerOffMotor.Size = new System.Drawing.Size(111, 60);
            this.btnPowerOffMotor.TabIndex = 25;
            this.btnPowerOffMotor.Text = "모터 전원 끄기";
            this.btnPowerOffMotor.UseVisualStyleBackColor = true;
            this.btnPowerOffMotor.Click += new System.EventHandler(this.btnPowerOffMotor_Click);
            // 
            // btnLeft100Steps
            // 
            this.btnLeft100Steps.Location = new System.Drawing.Point(23, 448);
            this.btnLeft100Steps.Name = "btnLeft100Steps";
            this.btnLeft100Steps.Size = new System.Drawing.Size(81, 63);
            this.btnLeft100Steps.TabIndex = 26;
            this.btnLeft100Steps.Text = "<<< (100)";
            this.btnLeft100Steps.UseVisualStyleBackColor = true;
            this.btnLeft100Steps.Click += new System.EventHandler(this.btnLeft100Steps_Click);
            // 
            // btnLeft10Steps
            // 
            this.btnLeft10Steps.Location = new System.Drawing.Point(110, 448);
            this.btnLeft10Steps.Name = "btnLeft10Steps";
            this.btnLeft10Steps.Size = new System.Drawing.Size(81, 63);
            this.btnLeft10Steps.TabIndex = 27;
            this.btnLeft10Steps.Text = "<< (10)";
            this.btnLeft10Steps.UseVisualStyleBackColor = true;
            this.btnLeft10Steps.Click += new System.EventHandler(this.btnLeft10Steps_Click);
            // 
            // btnLeft1Step
            // 
            this.btnLeft1Step.Location = new System.Drawing.Point(197, 448);
            this.btnLeft1Step.Name = "btnLeft1Step";
            this.btnLeft1Step.Size = new System.Drawing.Size(81, 63);
            this.btnLeft1Step.TabIndex = 28;
            this.btnLeft1Step.Text = "< (1)";
            this.btnLeft1Step.UseVisualStyleBackColor = true;
            this.btnLeft1Step.Click += new System.EventHandler(this.btnLeft1Step_Click);
            // 
            // btnRight1Step
            // 
            this.btnRight1Step.Location = new System.Drawing.Point(284, 448);
            this.btnRight1Step.Name = "btnRight1Step";
            this.btnRight1Step.Size = new System.Drawing.Size(81, 63);
            this.btnRight1Step.TabIndex = 29;
            this.btnRight1Step.Text = "(1) >";
            this.btnRight1Step.UseVisualStyleBackColor = true;
            this.btnRight1Step.Click += new System.EventHandler(this.btnRight1Step_Click);
            // 
            // btnRight10Steps
            // 
            this.btnRight10Steps.Location = new System.Drawing.Point(371, 448);
            this.btnRight10Steps.Name = "btnRight10Steps";
            this.btnRight10Steps.Size = new System.Drawing.Size(81, 63);
            this.btnRight10Steps.TabIndex = 30;
            this.btnRight10Steps.Text = "(10) >";
            this.btnRight10Steps.UseVisualStyleBackColor = true;
            this.btnRight10Steps.Click += new System.EventHandler(this.btnRight10Steps_Click);
            // 
            // btnRight100Steps
            // 
            this.btnRight100Steps.Location = new System.Drawing.Point(459, 448);
            this.btnRight100Steps.Name = "btnRight100Steps";
            this.btnRight100Steps.Size = new System.Drawing.Size(81, 63);
            this.btnRight100Steps.TabIndex = 31;
            this.btnRight100Steps.Text = "(100) >";
            this.btnRight100Steps.UseVisualStyleBackColor = true;
            this.btnRight100Steps.Click += new System.EventHandler(this.btnRight100Steps_Click);
            // 
            // listBoxLog
            // 
            this.listBoxLog.FormattingEnabled = true;
            this.listBoxLog.ItemHeight = 12;
            this.listBoxLog.Location = new System.Drawing.Point(680, 36);
            this.listBoxLog.Name = "listBoxLog";
            this.listBoxLog.Size = new System.Drawing.Size(354, 484);
            this.listBoxLog.TabIndex = 32;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(681, 11);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 33;
            this.label10.Text = "로그";
            // 
            // DeviceControlForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1046, 531);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.listBoxLog);
            this.Controls.Add(this.btnRight100Steps);
            this.Controls.Add(this.btnRight10Steps);
            this.Controls.Add(this.btnRight1Step);
            this.Controls.Add(this.btnLeft1Step);
            this.Controls.Add(this.btnLeft10Steps);
            this.Controls.Add(this.btnLeft100Steps);
            this.Controls.Add(this.btnPowerOffMotor);
            this.Controls.Add(this.btnPowerOnMotor);
            this.Controls.Add(this.btnFineControl);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbSteps);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btnStopMotion);
            this.Controls.Add(this.btnCVMDrive_Rotation);
            this.Controls.Add(this.btnDeviceSetting);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.tbAngularVelocityInRadian);
            this.Controls.Add(this.btnCVMDrive);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.tbVelocity);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox3);
            this.Name = "DeviceControlForm";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DeviceControlForm_FormClosing);
            this.Load += new System.EventHandler(this.DeviceControlForm_Load);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbVelocity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCVMDrive;
        private System.Windows.Forms.Button btnStopMotion;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnSinusoidalDrive;
        private System.Windows.Forms.TextBox tbSinusodialDrive_AmplitudeInMillimeters;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbSinusoidalDrive_FrequencyHz;
        private System.Windows.Forms.TextBox tbSinusoidalDrive_PhaseConstantInRadian;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnDeviceSetting;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox tbPhaseConstant_Rad_Rotation;
        private System.Windows.Forms.TextBox tbFrequency_Rotation;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbSinusoidalDrive_AngularAmp_Degrees_Rotation;
        private System.Windows.Forms.Button btnSinusoidalDrive_Rotation;
        private System.Windows.Forms.Button btnCVMDrive_Rotation;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbAngularVelocityInRadian;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox tbSteps;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnFineControl;
        private System.Windows.Forms.Button btnPowerOnMotor;
        private System.Windows.Forms.Button btnPowerOffMotor;
        private System.Windows.Forms.Button btnLeft100Steps;
        private System.Windows.Forms.Button btnLeft10Steps;
        private System.Windows.Forms.Button btnLeft1Step;
        private System.Windows.Forms.Button btnRight1Step;
        private System.Windows.Forms.Button btnRight10Steps;
        private System.Windows.Forms.Button btnRight100Steps;
        private System.Windows.Forms.ListBox listBoxLog;
        private System.Windows.Forms.Label label10;
    }
}

