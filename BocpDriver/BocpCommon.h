﻿
#pragma once

#include "BocpDefinitions.h"

namespace BocpDriver
{
    enum class BocpOperation : DWORD
    {
        Connect = 0,
        Disconnect = 1,
        QuerySpec = 2,
        QueryOscillationState = 3,
        QuerySetting = 4,
        UpdateSetting = 5,
        StartConstantVelocityMode = 6,
        StartHdmUsingSignal = 7,
        StartHdmUsingDerivative = 8,
        StopOscillation = 9,
        StopHostDriven = 10,
        StartSinusoidalDrive = 11,
        FineControl = 12,
        ControlMotorPower = 13
    };

    // BOCP event handler (applies to all events)
    typedef struct
    {
        DWORD eventType;
        LPCVOID lpParam; // only valid within the handler function body

    } BOCP_EVENT_HANDLER_DATA;
    typedef void (WINAPI *BOCP_NOTIFICATION_HANDLER) (const BOCP_EVENT_HANDLER_DATA *lpData);

    // BOCP HDM sample data provider
    typedef int64_t(WINAPI *BOCP_SAMPLE_DATA_PROVIDER) (int64_t timeInMicroseconds);

    typedef struct
    {
        uint64_t lastSampleIndex;

    } BOCP_OPERATION_RESULT_HDM_STOP;

    typedef struct
    {
        // 사용자가 함수 호출 시 설정한 값은 아무 의미 없음. 어차피 내부적으로 override됨.
        // 이 필드는 콜백 함수가 무슨 요청이 완료되었는지에 대한 호출인지를 판별할 수 있도록 하기 위한 것이다.
        BocpOperation operation;

        HANDLE hEvent; // INVALID_HANDLE_VALUE가 아니라면, 작업 완료 시 이벤트가 시그널됨
        BocpStatusCode status;
        DWORD lastWin32Error;
        LPVOID lpUser;

        union
        {
            BOCP_OPERATION_RESULT_HDM_STOP hdm_stop;

        } result;

    } BOCP_OPERATION_ARGS_BASE;

    typedef void (WINAPI *BOCP_CALLBACK) (const BOCP_OPERATION_ARGS_BASE *pArgs);

    // a sort of hack to prevent cyclic problem
    struct BOCP_OPERATION_ARGS : public BOCP_OPERATION_ARGS_BASE
    {
        BOCP_CALLBACK callback; // NULL이 아니면 작업 완료 후 호출된다.
    };
}
