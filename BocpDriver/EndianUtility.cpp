
#include "Stdafx.h"
#include "EndianUtility.h"

void WriteBigEndian16(uint8_t *buf, uint16_t value)
{
    buf[0] = (uint8_t)(value >> 8);
    buf[1] = (uint8_t)(value);
}
void WriteBigEndian32(uint8_t *buf, uint32_t value)
{
    buf[0] = (uint8_t)(value >> 24);
    buf[1] = (uint8_t)(value >> 16);
    buf[2] = (uint8_t)(value >> 8);
    buf[3] = (uint8_t)(value);
}

// for x86 or x86-64 systems
void WriteSinglePrecision(uint8_t *buf, float value)
{
    // write in big endian
    uint8_t *ptr = (uint8_t *)&value;
    buf[0] = ptr[0];
    buf[1] = ptr[1];
    buf[2] = ptr[2];
    buf[3] = ptr[3];
}
