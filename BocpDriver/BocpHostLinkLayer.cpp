
#include "stdafx.h"
#include "LinkLayerEncoder.h"
#include "LinkLayerDecoder.h"
#include "BocpHostLinkLayer.h"

#include "ScopedEventHandle.h"

#include <stdexcept>

namespace BocpDriver
{
    static const BYTE g_syncZeros[BOCP_SYNC_ZERO_BYTES] = { 0, };

    BocpHostLinkLayer::BocpHostLinkLayer()
    {
        m_packetSeqNum = 0;
        m_szDecoderFeed = 0;

        m_fWaitingForResponse = FALSE;
    }
    BocpHostLinkLayer::~BocpHostLinkLayer()
    {
        if (m_serial.IsOpen())
            m_serial.Close();
    }
    void BocpHostLinkLayer::Connect(LPCWSTR lpszDevice, LINK_LAYER_CONNECT_RESULT *pCr, HANDLE hCancellationEvent)
    {
        if (!pCr)
            throw std::invalid_argument(u8"Argument is nullptr");

        /*
        할 일:
          1) 시리얼 포트 열기
          2) Handshake
        */

        BOCPPACKET packet;
        BocpStatusCode status;

        if (IsConnected())
        {
            // 이미 열려있다면, 다시 열 수 없다.
            pCr->status = BOCP_E_INVALID_OPERATION;
            pCr->lastWin32Error = ERROR_INVALID_OPERATION;
            return;
        }

        // 시리얼 포트 열기
        if (!m_serial.Open(lpszDevice) ||
            !m_serial.Setup(BOCP_BAUD_RATE, DATABITS_8, PARITY_NONE, STOPBITS_10) ||// PARITY_EVEN, STOPBITS_10) ||
            !m_serial.SetupReadTimeoutMode(BocpSerial::EReadTimeoutNonblocking))
        {
            pCr->status = BOCP_E_SERIAL_IO;
            pCr->lastWin32Error = m_serial.GetLastError();

            if (m_serial.IsOpen()) m_serial.Close();
            return;
        }

        // Handshake 수행
        /*
        1) 동기화 (00) 전송
        2) SYN 전송
        3) SYN 응답 대기
        */

        if (BOCP_FAILED(status = SendSyncPattern(hCancellationEvent)) ||
            BOCP_FAILED(status = SendSYN(hCancellationEvent)) ||
            BOCP_FAILED(status = ReceiveFromDevice(&packet, hCancellationEvent)))
        {
            // ERROR
            pCr->status = status;
            pCr->lastWin32Error = m_serial.GetLastError();

            if (m_serial.IsOpen()) m_serial.Close();
            return;
        }

        if ((packet.Flags & BOCP_FLAGS_SYN) != BOCP_FLAGS_SYN)
        {
            // Got Invalid response
            pCr->status = BOCP_E_INVALID_RESPONSE;
            pCr->lastWin32Error = ERROR_SUCCESS;

            if (m_serial.IsOpen()) m_serial.Close();
            return;
        }

        // Handshake successful
        pCr->status = BOCP_E_SUCCESS;
        pCr->lastWin32Error = ERROR_SUCCESS;
    }
    void BocpHostLinkLayer::Send(LPCVOID lpMessage, size_t szMessage, LINK_LAYER_SEND_RESULT *pSr, HANDLE hCancellationEvent)
    {
        if (!lpMessage || szMessage > BOCP_MAX_PAYLOAD_SIZE || !pSr)
            throw std::invalid_argument(u8"Argument is nullptr");

        if (!IsConnected())
        {
            pSr->status = BOCP_E_INVALID_OPERATION;
            pSr->lastWin32Error = ERROR_INVALID_OPERATION;
            return;
        }

        BocpStatusCode status = SendMessageToDevice(lpMessage, szMessage, hCancellationEvent);
        if (BOCP_SUCCEEDED(status))
        {
            pSr->status = ERROR_SUCCESS;
            pSr->lastWin32Error = ERROR_SUCCESS;
        }
        else
        {
            pSr->status = status;
            pSr->lastWin32Error = m_serial.GetLastError();
        }
    }
    void BocpHostLinkLayer::Receive(LPVOID lpBuffer, LINK_LAYER_RECEIVE_RESULT *pRr, HANDLE hCancellationEvent)
    {
        if (!lpBuffer || !pRr)
            throw std::invalid_argument(u8"Argument is nullptr");

        if (!IsConnected())
        {
            pRr->status = BOCP_E_INVALID_OPERATION;
            pRr->lastWin32Error = ERROR_INVALID_OPERATION;
            return;
        }

        BOCPPACKET packet;
        BocpStatusCode status;

        // 디바이스로부터 응답 수신
        if (BOCP_FAILED(status = ReceiveFromDevice(&packet, hCancellationEvent)))
        {
            pRr->status = status;
            pRr->lastWin32Error = m_serial.GetLastError();
            return;
        }

        // 올바른 응답인지 확인
        if (packet.Flags & BOCP_FLAGS_SYN || packet.Flags & BOCP_FLAGS_FIN || packet.PayloadLength == 0)
        {
            pRr->status = BOCP_E_INVALID_RESPONSE;
            pRr->lastWin32Error = 0;
            return;
        }

        // NOTE: 공통 오류 응답인지 아닌지 확인하는 건
        // 상위 레이어의 몫이다.

        memcpy_s(lpBuffer, BOCP_MAX_PAYLOAD_SIZE, packet.Payload, packet.PayloadLength);
        pRr->Buffer = lpBuffer;
        pRr->ReceivedMessageLength = packet.PayloadLength;

        pRr->status = BOCP_E_SUCCESS;
        pRr->lastWin32Error = ERROR_SUCCESS;
    }
    void BocpHostLinkLayer::Disconnect(LINK_LAYER_DISCONNECT_RESULT *pDr, HANDLE hCancellationEvent)
    {
        if (!pDr)
            throw std::invalid_argument(u8"Argument is nullptr");

        if (!IsConnected())
        {
            pDr->status = BOCP_E_INVALID_OPERATION;
            pDr->lastWin32Error = ERROR_INVALID_OPERATION;
            return;
        }

        // 연결을 끊는 상황에서는, FIN은 그냥 보내놓고 잊어버려도 된다
        SendFIN(hCancellationEvent);

        m_serial.Close();

        pDr->status = BOCP_E_SUCCESS;
        pDr->lastWin32Error = ERROR_SUCCESS;
    }
    BOOL BocpHostLinkLayer::IsConnected()
    {
        return (m_serial.IsOpen());
    }
    BocpStatusCode BocpHostLinkLayer::SendSyncPattern(HANDLE hCancellationEvent, DWORD dwTimeout)
    {
        DWORD dwWritten;
        if (!m_serial.Write(g_syncZeros, sizeof(g_syncZeros), &dwWritten, dwTimeout, hCancellationEvent))
        {
            return (m_serial.GetLastError() == ERROR_CANCELLED) ? BOCP_E_CANCELED : BOCP_E_SERIAL_IO;
        }
        return BOCP_E_SUCCESS;
    }
    BocpStatusCode BocpHostLinkLayer::SendSYN(HANDLE hCancellationEvent)
    {
        return SendToDevice(NULL, 0, BOCP_FLAGS_SYN, hCancellationEvent);
    }
    BocpStatusCode BocpHostLinkLayer::SendFIN(HANDLE hCancellationEvent)
    {
        return SendToDevice(NULL, 0, BOCP_FLAGS_FIN, hCancellationEvent);
    }
    BocpStatusCode BocpHostLinkLayer::SendToDevice(LPCVOID lpBuffer, DWORD szPayload, BYTE flags, HANDLE hCancellationEvent, DWORD dwTimeout)
    {
        if (szPayload >= BOCP_MAX_PAYLOAD_SIZE)
            return BOCP_E_INVALID_PARAMETER;

        if (m_fWaitingForResponse)
            return BOCP_E_INVALID_OPERATION;

        BYTE stuffedBytes[BOCP_MAX_STUFFED_PACKET_SIZE];
        size_t szStuffed;
        LinkLayerEncoder::GenerateSavePacket(lpBuffer, szPayload, flags, m_packetSeqNum, stuffedBytes, &szStuffed);

        DWORD dwWritten;
        if (!m_serial.Write(stuffedBytes, szStuffed, &dwWritten, dwTimeout, hCancellationEvent))
        {
            return (m_serial.GetLastError() == ERROR_CANCELLED) ? BOCP_E_CANCELED : BOCP_E_SERIAL_IO;
        }
        if (dwWritten != szStuffed)
        {
            return BOCP_E_SERIAL_IO;
        }

        // 혹시나 있을지 모르는 재전송을 위해 현재 송신한 데이터를 저장
        m_lastPacketSent.PayloadLength = szPayload;
        m_lastPacketSent.Flags = flags;
        m_lastPacketSent.SeqNum = m_packetSeqNum;
        memcpy_s(m_lastPacketSent.Payload, sizeof(m_lastPacketSent.Payload), lpBuffer, szPayload);

        m_fWaitingForResponse = TRUE;

        return BOCP_E_SUCCESS;
    }
    BocpStatusCode BocpHostLinkLayer::ResendToDevice(HANDLE hCancellationEvent, DWORD dwTimeout)
    {
        if (!m_fWaitingForResponse)
            return BOCP_E_INVALID_OPERATION;

        BYTE stuffedBytes[BOCP_MAX_STUFFED_PACKET_SIZE];
        size_t szStuffed;
        LinkLayerEncoder::GenerateSavePacket(m_lastPacketSent.Payload, m_lastPacketSent.PayloadLength,
            m_lastPacketSent.Flags | BOCP_FLAGS_RTR, m_lastPacketSent.SeqNum, stuffedBytes, &szStuffed);
        DWORD dwWritten;
        if (!m_serial.Write(stuffedBytes, szStuffed, &dwWritten, dwTimeout, hCancellationEvent))
        {
            return (m_serial.GetLastError() == ERROR_CANCELLED) ? BOCP_E_CANCELED : BOCP_E_SERIAL_IO;
        }
        if (dwWritten != szStuffed)
        {
            return BOCP_E_SERIAL_IO;
        }

        return BOCP_E_SUCCESS;
    }
    BocpStatusCode BocpHostLinkLayer::SendMessageToDevice(LPCVOID lpBuffer, DWORD szMessage, HANDLE hCancellationEvent)
    {
        return SendToDevice(lpBuffer, szMessage, BOCP_FLAGS_NONE, hCancellationEvent);
    }
    BocpStatusCode BocpHostLinkLayer::ReceiveFromDevice(LPBOCPPACKET lpReceivedPacket, HANDLE hCancellationEvent)
    {
        if (!m_fWaitingForResponse)
            return BOCP_E_INVALID_OPERATION;

        DWORD dwStartTimestamp = ::GetTickCount();
        DWORD dwRemainingMilliseconds;
        BocpStatusCode status;

        // 시퀀스 번호가 제대로 될 때까지 반복
        while (1)
        {
            status = ReceiveFromDeviceCore(lpReceivedPacket, dwStartTimestamp, hCancellationEvent);
            if (BOCP_FAILED(status))
                return status;

            // seq num 일치하는지 확인
            if (lpReceivedPacket->SeqNum != m_packetSeqNum)
            {
                // sequence number mismatch
                // 해야 하는 일: 직전에 보냈던 메시지를 다시 보내고 재수신

                // retransmit
                dwRemainingMilliseconds = BOCP_CONNECTION_TIMEOUT_MILLISECONDS - (::GetTickCount() - dwStartTimestamp);
                if (dwRemainingMilliseconds > 0x80000000)
                {
                    // already timed out
                    m_fWaitingForResponse = FALSE;
                    return BOCP_E_TIMEOUT;
                }

                if (BOCP_FAILED(status = SendSyncPattern(hCancellationEvent)) ||
                    BOCP_FAILED(status = ResendToDevice(hCancellationEvent, dwRemainingMilliseconds)))
                {
                    m_fWaitingForResponse = FALSE;
                    return status;
                }
            }
            else
            {
                // 일치!
                m_fWaitingForResponse = FALSE;
                m_packetSeqNum++; // this will automatically wrap
                return BOCP_E_SUCCESS;
            }
        }
    }
    BocpStatusCode BocpHostLinkLayer::ReceiveFromDeviceCore(LPBOCPPACKET lpReceivedPacket, DWORD dwStartTimestamp, HANDLE hCancellationEvent)
    {
        /*
        1) 1바이트씩 받아 LinkLayerDecoder에 넣는다
        2) 넣다 보면 LinkLayerDecoder가 "packet ready" 하거나
        아니면 BOCP 타임아웃.
        -> 타임아웃을 구현하려면, 시작시점을 찍어놓는 수밖에 없다.
        (한 번 메시지를 수신한 뒤 안 끝날 수도 있다)
        3) Serial쪽에 hCancellationEvent도 처리한다
        */

        BYTE buffer[BOCP_MAX_STUFFED_PACKET_SIZE];
        DWORD dwRead;
        size_t dwBytesProcessed;
        DWORD dwRemainingMilliseconds;

        // 0) 이전에 남아있던 데이터 feed
        if (m_linkLayerDecoder.FeedInput(m_decoderFeed, m_szDecoderFeed, &dwBytesProcessed))
        {
            // 이전에 남아있던 데이터로도 충분히 처리되었음

            // packet ready
            lpReceivedPacket->PayloadLength = m_linkLayerDecoder.PacketGetPayloadLength();
            lpReceivedPacket->Flags = m_linkLayerDecoder.PacketGetFlags();
            lpReceivedPacket->SeqNum = m_linkLayerDecoder.PacketGetSeqNum();
            memcpy_s(lpReceivedPacket->Payload, sizeof(lpReceivedPacket->Payload),
                m_linkLayerDecoder.PacketGetPayloadBuffer(), m_linkLayerDecoder.PacketGetPayloadLength());

            // reset the decoder
            m_linkLayerDecoder.Reset();

            // 처리된 만큼 없앤다
            memmove_s(m_decoderFeed, sizeof(m_decoderFeed), m_decoderFeed + dwBytesProcessed, m_szDecoderFeed - dwBytesProcessed);
            m_szDecoderFeed -= dwBytesProcessed;

            return BOCP_E_SUCCESS;
        }
        else
        {
            // 이전에 남았던 데이터가 모두 사용되었다
            m_szDecoderFeed = 0;
        }

        while (1)
        {
            dwRemainingMilliseconds = BOCP_CONNECTION_TIMEOUT_MILLISECONDS - (::GetTickCount() - dwStartTimestamp);
            if (dwRemainingMilliseconds > 0x80000000)
            {
                // already timed out
                return BOCP_E_TIMEOUT;
            }

            if (!m_serial.Read(buffer, BOCP_MAX_STUFFED_PACKET_SIZE, &dwRead, dwRemainingMilliseconds, hCancellationEvent))
            {
                switch (m_serial.GetLastError())
                {
                case ERROR_TIMEOUT:
                    m_linkLayerDecoder.Reset();
                    return BOCP_E_TIMEOUT;
                case ERROR_CANCELLED:
                    m_linkLayerDecoder.Reset();
                    return BOCP_E_CANCELED;
                default:
                    // failed (probably Serial I/O problem)
                    // retry until timeout
                    break;
                }
                continue;
            }

            // communication successful -> decode
            if (m_linkLayerDecoder.FeedInput(buffer, dwRead, &dwBytesProcessed))
            {
                // packet ready 
                lpReceivedPacket->PayloadLength = m_linkLayerDecoder.PacketGetPayloadLength();
                lpReceivedPacket->Flags = m_linkLayerDecoder.PacketGetFlags();
                lpReceivedPacket->SeqNum = m_linkLayerDecoder.PacketGetSeqNum();
                memcpy_s(lpReceivedPacket->Payload, sizeof(lpReceivedPacket->Payload),
                    m_linkLayerDecoder.PacketGetPayloadBuffer(), m_linkLayerDecoder.PacketGetPayloadLength());

                // reset the decoder
                m_linkLayerDecoder.Reset();

                // save rest of the message for further processing
                memcpy_s(m_decoderFeed, sizeof(m_decoderFeed), buffer, dwRead - dwBytesProcessed);
                m_szDecoderFeed = dwRead - dwBytesProcessed;

                return BOCP_E_SUCCESS;
            }
        }
    }
}
