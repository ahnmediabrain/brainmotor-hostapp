﻿
#include "Stdafx.h"
#include "BocpSerial.h"

#include "ScopedEventHandle.h"

namespace BocpDriver
{
    // extension of OVERLAPPED structure
    //struct MY_OVERLAPPED : public OVERLAPPED
    //{
    //    uint8_t *buffer;
    //};

    //void WINAPI BocpSerial_CompletionCallback(DWORD dwErrorCode, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED lpOverlapped);

    BocpSerial::BocpSerial()
    {
        m_hSerial = INVALID_HANDLE_VALUE;
        m_lastError = ERROR_SUCCESS;
    }
    BocpSerial::~BocpSerial()
    {
        if (m_hSerial != INVALID_HANDLE_VALUE)
            CloseHandle(m_hSerial);
    }
    BOOL BocpSerial::Open(LPCWSTR lpszDevice, DWORD dwInQueue, DWORD dwOutQueue, BOOL fOverlapped)
    {
        if (m_hSerial != INVALID_HANDLE_VALUE)
        {
            m_lastError = ERROR_ALREADY_INITIALIZED;
            return FALSE;
        }

        HANDLE hSerial = ::CreateFileW(lpszDevice,
            GENERIC_READ | GENERIC_WRITE,
            0, NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL
            );
        if (hSerial == INVALID_HANDLE_VALUE)
        {
            // Obtain the error code
            m_lastError = ::GetLastError();
            return FALSE;
        }

        m_hSerial = hSerial;
        m_lastError = ERROR_SUCCESS;
        return TRUE;
    }
    BOOL BocpSerial::SetupReadTimeoutMode(EReadTimeoutMode eReadTimeoutMode)
    {
        if (m_hSerial == INVALID_HANDLE_VALUE)
        {
            m_lastError = ERROR_INVALID_HANDLE;
            return FALSE;
        }

        COMMTIMEOUTS cto;
        if (!::GetCommTimeouts(m_hSerial, &cto))
        {
            m_lastError = GetLastError();
            return FALSE;
        }

        switch (eReadTimeoutMode)
        {
        case EReadTimeoutMode::EReadTimeoutBlocking:
            cto.ReadIntervalTimeout = 0;
            cto.ReadTotalTimeoutConstant = 0;
            cto.ReadTotalTimeoutMultiplier = 0;
            break;
        case EReadTimeoutMode::EReadTimeoutNonblocking:
            cto.ReadIntervalTimeout = MAXDWORD;
            cto.ReadTotalTimeoutConstant = 0;
            cto.ReadTotalTimeoutMultiplier = 0;
            break;
        default:
            m_lastError = ERROR_BAD_ARGUMENTS;
            return FALSE;
        }

        if (::SetCommTimeouts(m_hSerial, &cto) == FALSE)
        {
            // Obtain the error code
            m_lastError = ::GetLastError();
            return FALSE;
        }

        m_lastError = ERROR_SUCCESS;
        return TRUE;
    }
    BOOL BocpSerial::Setup(DWORD baudrate, BYTE dataBits, BYTE parity, BYTE stopBits)
    {
        if (m_hSerial == INVALID_HANDLE_VALUE)
        {
            m_lastError = ERROR_INVALID_HANDLE;
            return FALSE;
        }

        // Obtain the DCB structure for the device
        DCB dcb;
        if (!::GetCommState(m_hSerial, &dcb))
        {
            // Obtain the error code
            m_lastError = ::GetLastError();
            return FALSE;
        }

        // Set the new data
        dcb.BaudRate = DWORD(baudrate);
        dcb.ByteSize = BYTE(dataBits);
        dcb.Parity = BYTE(parity);
        dcb.StopBits = BYTE(stopBits);

        // Determine if parity is used
        dcb.fParity = (parity != PARITY_NONE);

        // Set the new DCB structure
        if (!::SetCommState(m_hSerial, &dcb))
        {
            // Obtain the error code
            m_lastError = ::GetLastError();
            return FALSE;
        }

        m_lastError = ERROR_SUCCESS;
        return TRUE;
    }
    /*
    SerialConnectionState BocpSerial::GetConnectionState()
    {
        return SerialConnectionState();
    }*/
    BOOL BocpSerial::Read(LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead, DWORD dwTimeout, HANDLE hCancellationEvent)
    {
        if (m_hSerial == INVALID_HANDLE_VALUE)
        {
            m_lastError = ERROR_INVALID_HANDLE;
            return FALSE;
        }

        ScopedEventHandle eventHandle(FALSE, FALSE);

        OVERLAPPED ov;
        memset(&ov, 0, sizeof(ov));
        ov.hEvent = eventHandle.Handle();

        if (!::ReadFile(m_hSerial, lpBuffer, nNumberOfBytesToRead, lpNumberOfBytesRead, &ov))
        {
            // FALSE 반환값은
            //   GetLastError()가 ERROR_IO_PENDING => 비동기 작업 진행 중
            //                    다른 오류 코드   => I/O 에러
            DWORD dwError = ::GetLastError();
            if (dwError != ERROR_IO_PENDING)
            {
                // 오류 코드 저장
                m_lastError = dwError;
                return FALSE;
            }

            HANDLE handles[2] = { ov.hEvent, hCancellationEvent };
            switch (::WaitForMultipleObjects(sizeof(handles) / sizeof(handles[0]), handles, FALSE, dwTimeout))
            {
            case WAIT_OBJECT_0:
                // 비동기 동작이 완료됨
                if (!::GetOverlappedResult(m_hSerial, &ov, lpNumberOfBytesRead, FALSE))
                {
                    m_lastError = ::GetLastError(); // Event가 signal되었으므로 ERROR_IO_INCOMPLETE일 일은 없음
                    return FALSE;
                }
                m_lastError = ERROR_SUCCESS;
                return TRUE;
            case WAIT_TIMEOUT:
                // timed out
                ::CancelIoEx(m_hSerial, &ov); // CancelIoEx 자체의 성공/실패 여부는 안 중요함. 어차피 I/O가 완료될 때까지 기다려야 함
                ::WaitForSingleObject(ov.hEvent, INFINITE);
                m_lastError = ERROR_TIMEOUT;
                return FALSE;
            case WAIT_OBJECT_0 + 1:
                // canceled
                ::CancelIoEx(m_hSerial, &ov); // CancelIoEx 자체의 성공/실패 여부는 안 중요함. 어차피 I/O가 완료될 때까지 기다려야 함
                ::WaitForSingleObject(ov.hEvent, INFINITE);
                m_lastError = ERROR_CANCELLED;
                return FALSE;
            default:
                // unknown error
                m_lastError = ERROR_WAIT_1;
                return FALSE;
            }
        }
        else
        {
            // I/O가 즉시 (동기적으로) 완료됨
            return TRUE;
        }
    }
    BOOL BocpSerial::Write(LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten, DWORD dwTimeout, HANDLE hCancellationEvent)
    {
        if (m_hSerial == INVALID_HANDLE_VALUE)
        {
            m_lastError = ERROR_INVALID_HANDLE;
            return FALSE;
        }

        ScopedEventHandle eventHandle(FALSE, FALSE);

        OVERLAPPED ov;
        memset(&ov, 0, sizeof(ov));
        ov.hEvent = eventHandle.Handle();

        if (!::WriteFile(m_hSerial, lpBuffer, nNumberOfBytesToWrite, lpNumberOfBytesWritten, &ov))
        {
            // FALSE 반환값은
            //   GetLastError()가 ERROR_IO_PENDING => 비동기 작업 진행 중
            //                    다른 오류 코드   => I/O 에러
            DWORD dwError = ::GetLastError();
            if (dwError != ERROR_IO_PENDING)
            {
                // 오류 코드 저장
                m_lastError = dwError;
                return FALSE;
            }

            HANDLE handles[2] = { ov.hEvent, hCancellationEvent };
            switch (::WaitForMultipleObjects(sizeof(handles) / sizeof(handles[0]), handles, FALSE, dwTimeout))
            {
            case WAIT_OBJECT_0:
                // 비동기 동작이 완료됨
                if (!::GetOverlappedResult(m_hSerial, &ov, lpNumberOfBytesWritten, FALSE))
                {
                    m_lastError = ::GetLastError(); // Event가 signal되었으므로 ERROR_IO_INCOMPLETE일 일은 없음
                    return FALSE;
                }
                m_lastError = ERROR_SUCCESS;
                return TRUE;
            case WAIT_TIMEOUT:
                // timed out
                ::CancelIoEx(m_hSerial, &ov); // CancelIoEx 자체의 성공/실패 여부는 안 중요함. 어차피 I/O가 완료될 때까지 기다려야 함
                ::WaitForSingleObject(ov.hEvent, INFINITE);
                m_lastError = ERROR_TIMEOUT;
                return FALSE;
            case WAIT_OBJECT_0 + 1:
                // canceled
                ::CancelIoEx(m_hSerial, &ov); // CancelIoEx 자체의 성공/실패 여부는 안 중요함. 어차피 I/O가 완료될 때까지 기다려야 함
                ::WaitForSingleObject(ov.hEvent, INFINITE);
                m_lastError = ERROR_CANCELLED;
                return FALSE;
            default:
                // unknown error
                m_lastError = ERROR_WAIT_1;
                return FALSE;
            }
        }
        else
        {
            // I/O가 즉시 (동기적으로) 완료됨
            return TRUE;
        }
    }
    void BocpSerial::Close()
    {
        if (m_hSerial != INVALID_HANDLE_VALUE)
        {
            if (!CloseHandle(m_hSerial))
            {
                m_lastError = GetLastError();
                return;
            }
            m_hSerial = INVALID_HANDLE_VALUE;
        }
        m_lastError = ERROR_SUCCESS;
    }
    DWORD BocpSerial::GetLastError()
    {
        return m_lastError;
    }
    BOOL BocpSerial::IsOpen()
    {
        return (m_hSerial != INVALID_HANDLE_VALUE);
    }

    //void WINAPI BocpSerial_CompletionCallback(DWORD dwErrorCode, DWORD dwNumberOfBytesTransferred, LPOVERLAPPED lpOverlapped)
    //{
    //}
}
