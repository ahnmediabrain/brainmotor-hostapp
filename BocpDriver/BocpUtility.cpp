
#include "StdAfx.h"
#include "BocpUtility.h"

uint64_t micros()
{
    LARGE_INTEGER li, lf;
    QueryPerformanceCounter(&li);
    QueryPerformanceFrequency(&lf);

    // 1 second = 1,000,000 microseconds
    return li.QuadPart * 1000000 / lf.QuadPart;
}

uint32_t BocpFreqEncode(double Hz)
{
    uint32_t freq = 0;
    if (Hz > 0) freq |= 0x80000000; // ��ȣ ��Ʈ

    freq |= (uint32_t)(fabs(Hz) * 256) & 0x7FFFFFFF;
    return freq;
}
