
#include "Stdafx.h"
#include "LinkLayerEncoder.h"

#include "BocpDefinitions.h"
#include "COBSEncoder.h"
#include "Crc16_CCITT.h"

LinkLayerEncoder::LinkLayerEncoder()
{
}
LinkLayerEncoder::~LinkLayerEncoder()
{
}

bool LinkLayerEncoder::GenerateSavePacket(const void *payload, size_t payload_len, uint8_t flags, uint8_t seqNum, void *out, size_t *written)
{
    if (payload_len > BOCP_MAX_PAYLOAD_SIZE)
        return false;

    uint8_t packet[BOCP_MAX_PACKET_SIZE];
    size_t packet_length;

    packet[BOCP_PAYLOADLENGTH_FIELD_OFFSET] = (uint8_t)payload_len;
    packet[BOCP_FLAGS_FIELD_OFFSET] = flags;
    packet[BOCP_SEQNUM_FIELD_OFFSET] = seqNum;
    if (payload_len > 0) memcpy(packet + BOCP_HEADER_SIZE, payload, payload_len);

    uint16_t crc16 = Crc16_CCITT(packet, payload_len + BOCP_HEADER_SIZE);
    packet[payload_len + BOCP_HEADER_SIZE + 0] = (uint8_t)(crc16 >> 8);
    packet[payload_len + BOCP_HEADER_SIZE + 1] = (uint8_t)(crc16);
    packet[payload_len + BOCP_HEADER_SIZE + 2] = 0x00;
    packet_length = payload_len + BOCP_FRAME_SIZE;

    COBSEncoder cobsEncoder;
    if (!cobsEncoder.SetInputBuffer(packet, packet_length))
        return false;

    uint8_t *out_buf = (uint8_t *)out;
    int nextByte;
    while (true)
    {
        nextByte = cobsEncoder.GetNextByte();
        if (nextByte == -1)
            break;

        *out_buf++ = (uint8_t)nextByte;
    }

    *written = out_buf - (uint8_t *)out;
    return true;
}
