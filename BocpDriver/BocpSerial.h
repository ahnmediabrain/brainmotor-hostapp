﻿
#pragma once

namespace BocpDriver
{
    /*public enum class SerialConnectionState
    {
        NotConnected,
        Connected
    };
*/
    // BOCP I/O callbacks are called when the requested I/O is completed or cancelled
    //typedef void (WINAPI *BOCPIOCALLBACK) (
    //    DWORD dwNumberOfBytesTransferred,
    //    LPVOID lpUser
    //    );

    class BocpSerial
    {
    public:
        // Timeout settings
        typedef enum
        {
            EReadTimeoutNonblocking = 0,	// Always return immediately
            EReadTimeoutBlocking = 1	// Block until everything is retrieved
        }
        EReadTimeoutMode;

    public:
        BocpSerial();
        virtual ~BocpSerial();

    public:
        BOOL Open(LPCWSTR lpszDevice, DWORD dwInQueue = 0, DWORD dwOutQueue = 0, BOOL fOverlapped = true);
        BOOL SetupReadTimeoutMode(EReadTimeoutMode eReadTimeoutMode);
        BOOL Setup(DWORD baudrate, BYTE dataBits, BYTE parity, BYTE stopBits);
        //SerialConnectionState GetConnectionState();
        
        // Read, Write는 타임아웃을 제대로 지원한다.
        // 타임아웃된 경우, 마지막 오류 코드가 ERROR_TIMEOUT으로 설정된다.
        // 작업이 취소된 경우, 마지막 오류 코드가 ERROR_CANCELLED로 설정된다.
        BOOL Read(LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead,
            DWORD dwTimeout = INFINITE, HANDLE hCancellationEvent = INVALID_HANDLE_VALUE);
        BOOL Write(LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten,
            DWORD dwTimeout = INFINITE, HANDLE hCancellationEvent = INVALID_HANDLE_VALUE);
        void Close();

        DWORD GetLastError();

        BOOL IsOpen();

    private:
        HANDLE m_hSerial;
        DWORD m_lastError;
    };
}
