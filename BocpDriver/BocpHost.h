
#pragma once

#include "BocpCommon.h"
#include "BocpInternalDecl.h"
#include "BocpHostLinkLayer.h"

namespace BocpDriver
{
    struct BOCPHOST_INTERNAL;
    class BocpHost
    {
    public:
        // 객체 생성 즉시 백그라운드 스레드를 생성한다.
        BocpHost();
        ~BocpHost();

    public:
        bool IsConnected();

    public:
        // 디바이스와 통신하는 기능

        // NOTE: LPCWSTR lpszSerialDevice는 내부적으로 복사되므로 함수 반환 이후에 유지할 필요 없음
        void ConnectAsync(LPCWSTR lpszSerialDevice, BOCP_OPERATION_ARGS *pArgs); // 연결 + handshake 수행
        void DisconnectAsync(BOCP_OPERATION_ARGS *pArgs);

        void ControlMotorPowerAsync(BOOL fPowerOn, BOCP_OPERATION_ARGS *pArgs);
        void FineControlAsync(int32_t steps, BOCP_OPERATION_ARGS *pArgs);
        void StartLinearConstantVelocityModeAsync(double micrometersPerSecond, BOCP_OPERATION_ARGS *pArgs);
        void StartRotationalConstantVelocityModeAsync(double microradiansPerSecond, BOCP_OPERATION_ARGS *pArgs);
        void StartLinearSinusoidalDriveAsync(double amplitudeInMicrometers, double angularFrequencyInRadPerSec, double phaseConstantInRad, BOCP_OPERATION_ARGS *pArgs);
        void StartRotationalSinusoidalDriveAsync(double amplitudeInMicroradians, double angularFrequencyInRadPerSec, double phaseConstantInRad, BOCP_OPERATION_ARGS *pArgs);
        // Signal은 [um]의 단위를 가짐 (* 주기: 없으면 0으로 지정)
        void StartHdmUsingSignalAsync(BOCP_SAMPLE_DATA_PROVIDER signalFunction, double periodInMicroseconds, BOCP_OPERATION_ARGS *pArgs);
        // Derivative는 [um/s]의 단위를 가짐 (* 주기: 없으면 0으로 지정)
        void StartHdmUsingDerivativeAsync(BOCP_SAMPLE_DATA_PROVIDER signalDerivative, double periodInMicroseconds, BOCP_OPERATION_ARGS *pArgs);
        void StopOscillationAsync(BOCP_OPERATION_ARGS *pArgs); // HDM이었다면 마지막 샘플 인덱스를 통보해주도록 한다.

        void QuerySpecAsync(uint8_t specQueryCode, BOCP_OPERATION_ARGS *pArgs);
        void QueryOscillationStateAsync(uint8_t stateQueryCode, BOCP_OPERATION_ARGS *pArgs);
        void QuerySettingAsync(uint8_t settingCode, BOCP_OPERATION_ARGS *pArgs);
        void UpdateSettingAsync(uint8_t settingCode, uint32_t newValue, BOCP_OPERATION_ARGS *pArgs);

        // 명령에 대한 응답이 아닌, 예외적인 상황에 대한 통지를 받을
        // 이벤트 핸들러를 지정한다. (이벤트 핸들러는 백그라운드 스레드에서 호출된다)
        void SetNotificationHandler(BOCP_NOTIFICATION_HANDLER handler);
        BOCP_NOTIFICATION_HANDLER GetNotificationHandler();

    private:
        // 큐에 커맨드를 넣는다.
        // 먼저 입력된 커맨드가 먼저 처리된다(FIFO).
        // * 입력 매개변수의 유효성 검사는 BocpHost에서 한다.
        // * 현재 백그라운드 스레드가 자고 있다면 깨워야 한다.
        BOOL EnqueueCommand(BOCP_COMMAND &command);

        // 현재 실행중인 커맨드를 취소하고 큐에 남아있는 모든 커맨드를 없앤다.
        //void PurgeCommand();

    private:
        static void BackgroundWorkerLoopCaller(BocpHost *pThis);
        void BackgroundWorkerLoop();
        bool CheckOperationPossibility(BocpOperation operation);
        void ProcessCommand(BOCP_COMMAND &command, bool *pbCanceled);

        // 실제로 명령을 처리하는 루틴들.
        // * 여기서는 호출 측의 이벤트를 signal하지 않도록 한다. (ProcessCommand에서 signal한다)
        void ProcessCommand_Connect(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_Disconnect(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_StartConstantVelocityMode(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_StartHdmUsingSignal(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_StartHdmUsingDerivative(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_StopOscillation(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_QuerySpec(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_QueryOscillationState(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_QuerySetting(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_UpdateSetting(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_StartSinusoidalDrive(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_FineControl(BOCP_COMMAND &command, bool *pbCanceled);
        void ProcessCommand_ControlMotorPower(BOCP_COMMAND &command, bool *pbCanceled);

    private:
        BOOL IsCommonErrorResponse(LPVOID lpBuffer, DWORD szMessage, BocpStatusCode *pStatus);

    private:
        volatile bool m_bConnected;

        // this event is used to wake up background thread if it is sleeping
        HANDLE m_hWakeEvent;
        HANDLE m_hShutdownEvent;
        BOCP_NOTIFICATION_HANDLER m_pEventHandler;

        // 이 헤더 파일에 C++ 표준 멀티스레딩 라이브러리가 등장하면
        // 이 헤더를 포함하는 BocpHostDotNet.cpp를 CLR로 컴파일할 수 없다.
        // => 구조체 (opaque type) 로 wrap
        BOCPHOST_INTERNAL *m_pInternalData;

        //std::thread *m_backgroundWorker;

        //BocpCommandQueue *m_pCmdQueue;
        BocpHostLinkLayer m_linkLayer;
        OscillationMode m_currentMode;

        DWORD m_settingTable[BOCP_SETTING_COUNT_MAX];
        DWORD m_settingCount;
    };
}
