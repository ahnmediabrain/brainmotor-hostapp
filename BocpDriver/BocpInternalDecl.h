﻿
#pragma once

#include <string>
#include "BocpCommon.h"

namespace BocpDriver
{
    typedef struct
    {
        wchar_t lpszSerialDevice[MAX_PATH + 1];

    } BOCP_COMMAND_PARAMS_CONNECT;
    typedef struct
    {
        BOOL bLinearMotion;
        double micrometersPerSecond;
        double microradianPerSecond;

    } BOCP_COMMAND_PARAMS_START_CVM;
    typedef struct
    {
        BOOL bLinearMotion;
        double amplitudeInMicrometers;
        double amplitudeInMicroradians;
        double angularFrequencyInRadPerSec;
        double phaseConstantInRad;

    } BOCP_COMMAND_PARAMS_START_PWOM;
    typedef struct
    {
        int32_t steps;

    } BOCP_COMMAND_PARAMS_FINE_CONTROL;
    typedef struct
    {
        BOOL fPowerOn;

    } BOCP_COMMAND_PARAMS_CONTROL_MOTOR_POWER;

    typedef struct
    {
        BOCP_OPERATION_ARGS *pArgs;
        BOCP_CALLBACK callback; // 백그라운드 스레드에서 호출됨
        union
        {
            BOCP_COMMAND_PARAMS_CONNECT connect;
            BOCP_COMMAND_PARAMS_START_CVM startConstantVelocityMode;
            BOCP_COMMAND_PARAMS_START_PWOM startSinusoidalDrive;
            BOCP_COMMAND_PARAMS_FINE_CONTROL fineControl;
            BOCP_COMMAND_PARAMS_CONTROL_MOTOR_POWER controlMotorPower;

        } params; // 사용자가 전달한 매개변수

    } BOCP_COMMAND;
}
