
#pragma once

#include <stdint.h>

void WriteBigEndian16(uint8_t *buf, uint16_t value);
void WriteBigEndian32(uint8_t *buf, uint32_t value);

void WriteSinglePrecision(uint8_t *buf, float value);
