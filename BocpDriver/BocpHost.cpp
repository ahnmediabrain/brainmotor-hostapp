
#include "stdafx.h"
#include "BocpDefinitions.h"
#include "BocpInternalDecl.h"
#include "BocpHost.h"

#include "BocpUtility.h"
#include "EndianUtility.h"

#include <thread>
#include "thread_safe_queue.hpp"

namespace BocpDriver
{
    typedef thread_safe_queue<BOCP_COMMAND> BocpCommandQueue;
    struct BOCPHOST_INTERNAL
    {
        std::thread BackgroundWorker;
        BocpCommandQueue CmdQueue;
    };

    BocpHost::BocpHost()
    {
    //    LinkLayerDecoder lld;
    //    //BYTE data[] = { 0x00, 0x01, 0x02, 0x01, 0x03, 0x33, 0x31, 0x00 };
    //    BYTE data[] = { 0x00, 0x02, 0x01, 0x05, 0x01, 0x24, 0x21, 0x63, 0x00 };
    //    size_t read;
    //    lld.FeedInput(data, sizeof(data), &read);
    //    if (!lld.IsPacketReady())
    //    {
    //        OutputDebugStringW(L"FAIL");
    //    }
    //    else
    //    {
    //        OutputDebugStringW(L"SUCCESS");
    //    }

        m_bConnected = false;
        m_hWakeEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL); // 자동 리셋 이벤트로 한다. (수동 리셋으로 하면 이 경우는 귀찮다)
        m_hShutdownEvent = ::CreateEvent(NULL, TRUE, FALSE, NULL); // 취소 이벤트는 자동으로 리셋되면 안 되고 여러 번 참조해야 될 수 있다.
        m_pInternalData = new BOCPHOST_INTERNAL;

        m_currentMode = OSC_MODE_STOPPED;

        // run background worker
        m_pInternalData->BackgroundWorker = std::thread(BocpHost::BackgroundWorkerLoopCaller, this);
    }

    BocpHost::~BocpHost()
    {
        // shutdown background worker
        if (m_pInternalData->BackgroundWorker.joinable())
        {
            SetEvent(m_hShutdownEvent);
            m_pInternalData->BackgroundWorker.join();
        }

        // other cleanups
        if (m_linkLayer.IsConnected())
        {
            LINK_LAYER_DISCONNECT_RESULT dr; // unused
            m_linkLayer.Disconnect(&dr, INVALID_HANDLE_VALUE);
        }

        CloseHandle(m_hShutdownEvent);
        CloseHandle(m_hWakeEvent);
        delete m_pInternalData;
    }
    bool BocpHost::IsConnected()
    {
        return m_bConnected;
    }
    void BocpHost::ConnectAsync(LPCWSTR lpszSerialDevice, BOCP_OPERATION_ARGS *pArgs)
    {
        pArgs->operation = BocpOperation::Connect;

        size_t len = wcslen(lpszSerialDevice);
        if (len > MAX_PATH)
        {
            pArgs->status = BOCP_E_INVALID_PARAMETER;
            pArgs->lastWin32Error = ERROR_INVALID_PARAMETER;

            if (pArgs->hEvent != INVALID_HANDLE_VALUE)
            {
                ::ResetEvent(pArgs->hEvent);
                ::SetEvent(pArgs->hEvent);
            }
            return;
        }

        BOCP_COMMAND command;
        wcscpy_s(command.params.connect.lpszSerialDevice, lpszSerialDevice);
        command.pArgs = pArgs;

        EnqueueCommand(command); // FIXME: check if this function returns FALSE
    }
    void BocpHost::DisconnectAsync(BOCP_OPERATION_ARGS *pArgs)
    {
        pArgs->operation = BocpOperation::Disconnect;

        BOCP_COMMAND command;
        command.pArgs = pArgs;

        EnqueueCommand(command);
    }
    void BocpHost::ControlMotorPowerAsync(BOOL fPowerOn, BOCP_OPERATION_ARGS *pArgs)
    {
        pArgs->operation = BocpOperation::ControlMotorPower;

        BOCP_COMMAND command;
        command.params.controlMotorPower.fPowerOn = fPowerOn;
        command.pArgs = pArgs;

        EnqueueCommand(command);
    }
    void BocpHost::FineControlAsync(int32_t steps, BOCP_OPERATION_ARGS *pArgs)
    {
        pArgs->operation = BocpOperation::FineControl;

        BOCP_COMMAND command;
        command.params.fineControl.steps = steps;
        command.pArgs = pArgs;

        EnqueueCommand(command);
    }
    void BocpHost::StartLinearConstantVelocityModeAsync(double micrometersPerSecond, BOCP_OPERATION_ARGS *pArgs)
    {
        pArgs->operation = BocpOperation::StartConstantVelocityMode;

        BOCP_COMMAND command;
        command.params.startConstantVelocityMode.bLinearMotion = TRUE;
        command.params.startConstantVelocityMode.micrometersPerSecond = micrometersPerSecond;
        command.pArgs = pArgs;

        EnqueueCommand(command);
    }
    void BocpHost::StartRotationalConstantVelocityModeAsync(double microradiansPerSecond, BOCP_OPERATION_ARGS *pArgs)
    {
        pArgs->operation = BocpOperation::StartConstantVelocityMode;

        BOCP_COMMAND command;
        command.params.startConstantVelocityMode.bLinearMotion = FALSE;
        command.params.startConstantVelocityMode.microradianPerSecond = microradiansPerSecond;
        command.pArgs = pArgs;

        EnqueueCommand(command);
    }
    void BocpHost::StartLinearSinusoidalDriveAsync(double amplitudeInMicrometers, double angularFrequencyInRadPerSec, double phaseConstantInRad, BOCP_OPERATION_ARGS *pArgs)
    {
        pArgs->operation = BocpOperation::StartSinusoidalDrive;

        BOCP_COMMAND command;
        command.params.startSinusoidalDrive.bLinearMotion = TRUE;
        command.params.startSinusoidalDrive.amplitudeInMicrometers = amplitudeInMicrometers;
        command.params.startSinusoidalDrive.angularFrequencyInRadPerSec = angularFrequencyInRadPerSec;
        command.params.startSinusoidalDrive.phaseConstantInRad = phaseConstantInRad;
        command.pArgs = pArgs;

        EnqueueCommand(command);
    }
    void BocpHost::StartRotationalSinusoidalDriveAsync(double amplitudeInMicroradians, double angularFrequencyInRadPerSec, double phaseConstantInRad, BOCP_OPERATION_ARGS *pArgs)
    {
        pArgs->operation = BocpOperation::StartSinusoidalDrive;

        BOCP_COMMAND command;
        command.params.startSinusoidalDrive.bLinearMotion = FALSE;
        command.params.startSinusoidalDrive.amplitudeInMicroradians = amplitudeInMicroradians;
        command.params.startSinusoidalDrive.angularFrequencyInRadPerSec = angularFrequencyInRadPerSec;
        command.params.startSinusoidalDrive.phaseConstantInRad = phaseConstantInRad;
        command.pArgs = pArgs;

        EnqueueCommand(command);
    }
    BOOL BocpHost::EnqueueCommand(BOCP_COMMAND &command)
    {
        if (!m_pInternalData->CmdQueue.push(command))
            return FALSE;

        // signal background thread
        SetEvent(m_hWakeEvent);
        return TRUE;
    }
    //void BocpHost::PurgeCommand()
    //{
    //    // * 구현상의 문제: 그냥 없애버리면, 이벤트를 기다리던 객체들은 절대 signal되지 않음
    //                        -> 취소되었다고 쓰고 event 다 signal해 준 다음에 제거해야 함
    //    SetEvent(m_hCancelEvent);

    //    // clear command queue
    //    BocpCommandQueue empty;
    //    std::swap(*m_pCmdQueue, empty);

    void BocpHost::StartHdmUsingSignalAsync(BOCP_SAMPLE_DATA_PROVIDER signalFunction, double periodInMicroseconds, BOCP_OPERATION_ARGS * pArgs)
    {
    }

    void BocpHost::StartHdmUsingDerivativeAsync(BOCP_SAMPLE_DATA_PROVIDER signalDerivative, double periodInMicroseconds, BOCP_OPERATION_ARGS * pArgs)
    {
    }

    void BocpHost::StopOscillationAsync(BOCP_OPERATION_ARGS *pArgs)
    {
        pArgs->operation = BocpOperation::StopOscillation;

        BOCP_COMMAND command;
        command.pArgs = pArgs;

        EnqueueCommand(command);
    }

    void BocpHost::QuerySpecAsync(uint8_t specQueryCode, BOCP_OPERATION_ARGS * pArgs)
    {
    }

    void BocpHost::QueryOscillationStateAsync(uint8_t stateQueryCode, BOCP_OPERATION_ARGS * pArgs)
    {
    }

    void BocpHost::QuerySettingAsync(uint8_t settingCode, BOCP_OPERATION_ARGS * pArgs)
    {
    }

    void BocpHost::UpdateSettingAsync(uint8_t settingCode, uint32_t newValue, BOCP_OPERATION_ARGS * pArgs)
    {
    }

    //    ResetEvent(m_hCancelEvent);
    //}
    void BocpHost::SetNotificationHandler(BOCP_NOTIFICATION_HANDLER handler)
    {
        m_pEventHandler = handler;
    }
    BOCP_NOTIFICATION_HANDLER BocpHost::GetNotificationHandler()
    {
        return m_pEventHandler;
    }
    void BocpHost::BackgroundWorkerLoopCaller(BocpHost *pThis)
    {
        pThis->BackgroundWorkerLoop();
    }
    void BocpHost::BackgroundWorkerLoop()
    {
        HANDLE handles[2] = { m_hWakeEvent, m_hShutdownEvent };
        BOCP_COMMAND command;
        bool bCanceled;

        while (true)
        {
            // FIXME: 5초 이상 메시지가 오가지 않으면 연결 종료로 간주하므로
            //        5초마다 메시지를 보내야 함 (alive ping(?)의 개념)
            switch (WaitForMultipleObjects(sizeof(handles) / sizeof(handles[0]), handles, FALSE, INFINITE))
            {
            case WAIT_OBJECT_0: // NOTE: wake event는 자동 리셋 이벤트다.
                // process requested commands until we run out of them
                while (m_pInternalData->CmdQueue.try_pop(command))
                {
                    ProcessCommand(command, &bCanceled);
                    if (bCanceled)
                        return;
                }
                break;
            case WAIT_OBJECT_0 + 1:
                // shutdown event is signaled
                return;
            case WAIT_ABANDONED_0:
            case WAIT_ABANDONED_0 + 1:
                // FIXME
                // failure? what should I do?
                // (even if I throw an exception here, nobody would catch it)
                break;
            }
        }
    }
    bool BocpHost::CheckOperationPossibility(BocpOperation operation)
    {
        switch (operation)
        {
        case BocpOperation::Connect:
            return (m_bConnected == false);
        case BocpOperation::Disconnect:
        case BocpOperation::StartConstantVelocityMode:
        case BocpOperation::StartHdmUsingSignal:
        case BocpOperation::StartHdmUsingDerivative:
        case BocpOperation::StopOscillation:
        case BocpOperation::QuerySpec:
        case BocpOperation::QueryOscillationState:
        case BocpOperation::QuerySetting:
        case BocpOperation::UpdateSetting:
        case BocpOperation::StartSinusoidalDrive:
        case BocpOperation::FineControl:
        case BocpOperation::ControlMotorPower:
            return (m_bConnected == true);
        default:
            return false;
        }
    }
    void BocpHost::ProcessCommand(BOCP_COMMAND &command, bool *pbCanceled)
    {
        // 우선은 false로 표시. (나중에 shutdown signal을 받으면 그때 true로 표시)
        *pbCanceled = false;

        if (command.pArgs->hEvent != INVALID_HANDLE_VALUE)
            ::ResetEvent(command.pArgs->hEvent);

        // 현재 상태에서 요청된 명령을 수행할 수 있는지 확인
        if (!CheckOperationPossibility(command.pArgs->operation))
        {
            command.pArgs->status = BOCP_E_INVALID_OPERATION;
            command.pArgs->lastWin32Error = ERROR_INVALID_OPERATION;
        }
        else
        {
            switch (command.pArgs->operation)
            {
            case BocpOperation::Connect:
                ProcessCommand_Connect(command, pbCanceled);
                break;
            case BocpOperation::Disconnect:
                ProcessCommand_Disconnect(command, pbCanceled);
                break;
            case BocpOperation::StartConstantVelocityMode:
                ProcessCommand_StartConstantVelocityMode(command, pbCanceled);
                break;
            case BocpOperation::StartHdmUsingSignal:
                ProcessCommand_StartHdmUsingSignal(command, pbCanceled);
                break;
            case BocpOperation::StartHdmUsingDerivative:
                ProcessCommand_StartHdmUsingDerivative(command, pbCanceled);
                break;
            case BocpOperation::StopOscillation:
                ProcessCommand_StopOscillation(command, pbCanceled);
                break;
            case BocpOperation::QuerySpec:
                ProcessCommand_QuerySpec(command, pbCanceled);
                break;
            case BocpOperation::QueryOscillationState:
                ProcessCommand_QueryOscillationState(command, pbCanceled);
                break;
            case BocpOperation::QuerySetting:
                ProcessCommand_QuerySetting(command, pbCanceled);
                break;
            case BocpOperation::UpdateSetting:
                ProcessCommand_UpdateSetting(command, pbCanceled);
                break;
            case BocpOperation::StartSinusoidalDrive:
                ProcessCommand_StartSinusoidalDrive(command, pbCanceled);
                break;
            case BocpOperation::FineControl:
                ProcessCommand_FineControl(command, pbCanceled);
                break;
            case BocpOperation::ControlMotorPower:
                ProcessCommand_ControlMotorPower(command, pbCanceled);
                break;
            default:
                command.pArgs->status = BOCP_E_UNSUPPORTED;
                command.pArgs->lastWin32Error = ERROR_UNSUPPORTED_TYPE;
                break;
            }
        }

        if (command.pArgs->hEvent != INVALID_HANDLE_VALUE)
            ::SetEvent(command.pArgs->hEvent);

        if (command.pArgs->callback != NULL)
            command.pArgs->callback(command.pArgs);
    }
    void BocpHost::ProcessCommand_Connect(BOCP_COMMAND &command, bool *pbCanceled)
    {
        // 여기서는 pbCanceled를 false로 굳이 표시하지 않아도 된다. (이 함수 진입 이전에 이미 표시되서 들어옴)
        BOCP_COMMAND_PARAMS_CONNECT connectParams = command.params.connect;

        LINK_LAYER_CONNECT_RESULT cr;
        LINK_LAYER_SEND_RESULT sr;
        LINK_LAYER_RECEIVE_RESULT rr;
        LINK_LAYER_DISCONNECT_RESULT dr;

        // 1. 연결
        // 2. 설정 쿼리 (요청 송신 + 응답 수신)

        m_linkLayer.Connect(connectParams.lpszSerialDevice, &cr, m_hShutdownEvent); // NOTE: timeout은 Link Layer에서 알아서 해 줌
        if (BOCP_FAILED(cr.status))
        {
            command.pArgs->status = cr.status;
            command.pArgs->lastWin32Error = cr.lastWin32Error;

            if (cr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 먼저 setting query를 해 놓는다.
        BYTE settingQueryCommand[] = { BOCP_MSG_SETTING_QUERY_1 };
        m_linkLayer.Send(&settingQueryCommand, sizeof(settingQueryCommand), &sr, m_hShutdownEvent);
        if (BOCP_FAILED(sr.status))
        {
            command.pArgs->status = sr.status;
            command.pArgs->lastWin32Error = sr.lastWin32Error;
            m_linkLayer.Disconnect(&dr, INVALID_HANDLE_VALUE);

            if (sr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 그리고 응답을 기다린다.
        BYTE buffer[BOCP_MAX_PAYLOAD_SIZE];
        m_linkLayer.Receive(buffer, &rr, m_hShutdownEvent);
        if (BOCP_FAILED(rr.status))
        {
            command.pArgs->status = rr.status;
            command.pArgs->lastWin32Error = rr.lastWin32Error;
            m_linkLayer.Disconnect(&dr, INVALID_HANDLE_VALUE);

            if (rr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 이제 해석한다.
        DWORD settingCount = buffer[1];
        if (buffer[0] != BOCP_MSG_SETTING_QUERY_2 ||
            settingCount > BOCP_SETTING_COUNT_MAX ||
            rr.ReceivedMessageLength != (settingCount * 4 + 2))
        {
            command.pArgs->status = BOCP_E_INVALID_RESPONSE;
            command.pArgs->lastWin32Error = 0;
            m_linkLayer.Disconnect(&dr, INVALID_HANDLE_VALUE);

            return;
        }

        // 디바이스의 설정을 내부 버퍼에 저장
        m_settingCount = settingCount;

        DWORD *pReceivedSettings = (DWORD *)(buffer + 2);
        for (DWORD i = 0; i < settingCount; i++)
        {
            m_settingTable[i] = ntohl(pReceivedSettings[i]);
        }

        // 설정 검증 (FIXME: 구현 추가할 것)
        //!ValidateFixSettingTable(m_settingTable, settingCount);

        // 완료!
        m_bConnected = TRUE;
        command.pArgs->status = BOCP_E_SUCCESS;
        command.pArgs->lastWin32Error = ERROR_SUCCESS;
    }
    void BocpHost::ProcessCommand_Disconnect(BOCP_COMMAND &command, bool *pbCanceled)
    {
        LINK_LAYER_DISCONNECT_RESULT dr;
        m_linkLayer.Disconnect(&dr, m_hShutdownEvent);

        command.pArgs->status = dr.status;
        command.pArgs->lastWin32Error = dr.lastWin32Error;
        if (dr.status == BOCP_E_CANCELED) *pbCanceled = true;

        m_bConnected = FALSE;
    }
    void BocpHost::ProcessCommand_StartConstantVelocityMode(BOCP_COMMAND &command, bool *pbCanceled)
    {
        /*
        할 일:
            1) CVM 명령 전송
            2) 응답 수신
            3) 해석 + 전송
        */

        BOCP_COMMAND_PARAMS_START_CVM params = command.params.startConstantVelocityMode;
        LINK_LAYER_SEND_RESULT sr;
        LINK_LAYER_RECEIVE_RESULT rr;
        BocpStatusCode status;

        // 주파수 계산
        uint32_t bocpFreq;
        if (params.bLinearMotion)
        {
            bocpFreq = BocpFreqEncode(params.micrometersPerSecond * m_settingTable[SETTING_CODE_PULSE_PER_REV] / m_settingTable[SETTING_CODE_DISTANCE_PER_REV]);
        }
        else // rotational motion
        {
            bocpFreq = BocpFreqEncode(params.microradianPerSecond * m_settingTable[SETTING_CODE_PULSE_PER_REV] * (1 / (2 * M_PI * 1000000)));
        }

        // 메시지 전송
        BYTE buf[BOCP_MAX_PAYLOAD_SIZE];
        buf[0] = BOCP_MSG_CONSTANT_VELOCITY_MOVE_1;
        WriteBigEndian32(buf + 1, bocpFreq);

        m_linkLayer.Send(buf, 5, &sr, m_hShutdownEvent);
        if (BOCP_FAILED(sr.status))
        {
            command.pArgs->status = sr.status;
            command.pArgs->lastWin32Error = sr.lastWin32Error;

            if (sr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 응답 수신
        m_linkLayer.Receive(buf, &rr, m_hShutdownEvent);
        if (BOCP_FAILED(rr.status))
        {
            command.pArgs->status = rr.status;
            command.pArgs->lastWin32Error = rr.lastWin32Error;

            if (rr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 응답 해석
        if (IsCommonErrorResponse(buf, rr.ReceivedMessageLength, &status)) // CER인 경우 그 포맷에 맞추어 상태 코드를 읽어서 status를 본다.
        {
            // CVM-2 응답을 전송받은 것이 아니라 got common error response; in fact, something is wrong.
        }
        else
        {
            if (rr.ReceivedMessageLength != 3 || buf[0] != BOCP_MSG_CONSTANT_VELOCITY_MOVE_2)
            {
                command.pArgs->status = BOCP_E_INVALID_RESPONSE;
                command.pArgs->lastWin32Error = 0;
                return;
            }
            status = ntohs(*(u_short *)(buf + 1));
        }

        m_currentMode = OSC_MODE_CONSTANT_VELOCITY;

        command.pArgs->status = status;
        command.pArgs->lastWin32Error = 0;
    }
    void BocpHost::ProcessCommand_StartHdmUsingSignal(BOCP_COMMAND &command, bool *pbCanceled)
    {
    }
    void BocpHost::ProcessCommand_StartHdmUsingDerivative(BOCP_COMMAND &command, bool *pbCanceled)
    {
    }
    void BocpHost::ProcessCommand_StopOscillation(BOCP_COMMAND &command, bool *pbCanceled)
    {
        /*
        할 일:
        1) 진동 정지 명령 전송
        2) 응답 수신
        3) 해석 + notify
        */

        LINK_LAYER_SEND_RESULT sr;
        LINK_LAYER_RECEIVE_RESULT rr;
        BocpStatusCode status;

        // 메시지 전송
        BYTE buf[BOCP_MAX_PAYLOAD_SIZE];
        buf[0] = BOCP_MSG_STOP_OSCILLATION_1;

        m_linkLayer.Send(buf, 1, &sr, m_hShutdownEvent);
        if (BOCP_FAILED(sr.status))
        {
            command.pArgs->status = sr.status;
            command.pArgs->lastWin32Error = sr.lastWin32Error;

            if (sr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 응답 수신
        m_linkLayer.Receive(buf, &rr, m_hShutdownEvent);
        if (BOCP_FAILED(rr.status))
        {
            command.pArgs->status = rr.status;
            command.pArgs->lastWin32Error = rr.lastWin32Error;

            if (rr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 응답 해석
        if (IsCommonErrorResponse(buf, rr.ReceivedMessageLength, &status)) // CER인 경우 그 포맷에 맞추어 상태 코드를 읽어서 status를 본다.
        {
            // 구동 정지-2 응답을 전송받은 것이 아니라 got common error response; in fact, something is wrong.
        }
        else
        {
            if (rr.ReceivedMessageLength != 3 || buf[0] != BOCP_MSG_STOP_OSCILLATION_2)
            {
                command.pArgs->status = BOCP_E_INVALID_RESPONSE;
                command.pArgs->lastWin32Error = 0;
                return;
            }
            status = ntohs(*(u_short *)(buf + 1));
        }

        m_currentMode = OSC_MODE_PREDEFINED_WAVEFORM;

        command.pArgs->status = status;
        command.pArgs->lastWin32Error = 0;
    }
    void BocpHost::ProcessCommand_QuerySpec(BOCP_COMMAND &command, bool *pbCanceled)
    {
    }
    void BocpHost::ProcessCommand_QueryOscillationState(BOCP_COMMAND &command, bool *pbCanceled)
    {
    }
    void BocpHost::ProcessCommand_QuerySetting(BOCP_COMMAND &command, bool *pbCanceled)
    {
    }
    void BocpHost::ProcessCommand_UpdateSetting(BOCP_COMMAND &command, bool *pbCanceled)
    {
    }
    void BocpHost::ProcessCommand_StartSinusoidalDrive(BOCP_COMMAND &command, bool *pbCanceled)
    {
        /*
        할 일:
        1) 진폭, 각진동수, 위상상수를 변환
        2) 명령 전송
        3) 응답 수신
        4) 해석 + notify
        */

        BOCP_COMMAND_PARAMS_START_PWOM params = command.params.startSinusoidalDrive;
        LINK_LAYER_SEND_RESULT sr;
        LINK_LAYER_RECEIVE_RESULT rr;
        BocpStatusCode status;

        // 매개변수 변환
        float amplitudeSteps;
        if (params.bLinearMotion)
        {
            amplitudeSteps = (float)(params.amplitudeInMicrometers * m_settingTable[SETTING_CODE_PULSE_PER_REV] / m_settingTable[SETTING_CODE_DISTANCE_PER_REV]);
        }
        else
        {
            amplitudeSteps = (float)(params.amplitudeInMicroradians * m_settingTable[SETTING_CODE_PULSE_PER_REV] * (1 / (2 * M_PI * 1000000)));
        }
        float angularFrequency = (float)(params.angularFrequencyInRadPerSec);
        float phaseConstant = (float)(params.phaseConstantInRad);

        // 메시지 전송
        BYTE buf[BOCP_MAX_PAYLOAD_SIZE];
        buf[0] = BOCP_MSG_PREDEFINED_WAVEFORM_OSCILLATION_MODE_1;
        buf[1] = WAVEFORM_SINUSOIDAL;
        WriteSinglePrecision(buf + 2, amplitudeSteps);
        WriteSinglePrecision(buf + 6, angularFrequency);
        WriteSinglePrecision(buf + 10, phaseConstant);

        m_linkLayer.Send(buf, 14, &sr, m_hShutdownEvent);
        if (BOCP_FAILED(sr.status))
        {
            command.pArgs->status = sr.status;
            command.pArgs->lastWin32Error = sr.lastWin32Error;

            if (sr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 응답 수신
        m_linkLayer.Receive(buf, &rr, m_hShutdownEvent);
        if (BOCP_FAILED(rr.status))
        {
            command.pArgs->status = rr.status;
            command.pArgs->lastWin32Error = rr.lastWin32Error;

            if (rr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 응답 해석
        if (IsCommonErrorResponse(buf, rr.ReceivedMessageLength, &status)) // CER인 경우 그 포맷에 맞추어 상태 코드를 읽어서 status를 본다.
        {
            // pair 응답을 전송받은 것이 아니라 got common error response; in fact, something is wrong.
        }
        else
        {
            if (rr.ReceivedMessageLength != 3 || buf[0] != BOCP_MSG_PREDEFINED_WAVEFORM_OSCILLATION_MODE_2)
            {
                command.pArgs->status = BOCP_E_INVALID_RESPONSE;
                command.pArgs->lastWin32Error = 0;
                return;
            }
            status = ntohs(*(u_short *)(buf + 1));
        }

        m_currentMode = OSC_MODE_PREDEFINED_WAVEFORM;

        command.pArgs->status = status;
        command.pArgs->lastWin32Error = 0;
    }
    void BocpHost::ProcessCommand_FineControl(BOCP_COMMAND &command, bool *pbCanceled)
    {
        /*
        할 일:
        1) 명령 전송
        2) 응답 수신
        3) 해석 + notify
        */

        BOCP_COMMAND_PARAMS_FINE_CONTROL params = command.params.fineControl;
        LINK_LAYER_SEND_RESULT sr;
        LINK_LAYER_RECEIVE_RESULT rr;
        BocpStatusCode status;

        if (abs(params.steps) > 1000000)
        {
            command.pArgs->status = BOCP_E_OUT_OF_RANGE;
            command.pArgs->lastWin32Error = 0;
            return;
        }

        // 메시지 전송
        BYTE buf[BOCP_MAX_PAYLOAD_SIZE];
        buf[0] = BOCP_MSG_FINE_CONTROL_1;
        WriteBigEndian32(buf + 1, params.steps);

        m_linkLayer.Send(buf, 5, &sr, m_hShutdownEvent);
        if (BOCP_FAILED(sr.status))
        {
            command.pArgs->status = sr.status;
            command.pArgs->lastWin32Error = sr.lastWin32Error;

            if (sr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 응답 수신
        m_linkLayer.Receive(buf, &rr, m_hShutdownEvent);
        if (BOCP_FAILED(rr.status))
        {
            command.pArgs->status = rr.status;
            command.pArgs->lastWin32Error = rr.lastWin32Error;

            if (rr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 응답 해석
        if (IsCommonErrorResponse(buf, rr.ReceivedMessageLength, &status)) // CER인 경우 그 포맷에 맞추어 상태 코드를 읽어서 status를 본다.
        {
            // pair 응답을 전송받은 것이 아니라 got common error response; in fact, something is wrong.
        }
        else
        {
            if (rr.ReceivedMessageLength != 3 || buf[0] != BOCP_MSG_FINE_CONTROL_2)
            {
                command.pArgs->status = BOCP_E_INVALID_RESPONSE;
                command.pArgs->lastWin32Error = 0;
                return;
            }
            status = ntohs(*(u_short *)(buf + 1));
        }

        command.pArgs->status = status;
        command.pArgs->lastWin32Error = 0;
    }
    void BocpHost::ProcessCommand_ControlMotorPower(BOCP_COMMAND &command, bool *pbCanceled)
    {
        /*
        할 일:
        1) 명령 전송
        2) 응답 수신
        3) 해석 + notify
        */

        BOCP_COMMAND_PARAMS_CONTROL_MOTOR_POWER params = command.params.controlMotorPower;
        LINK_LAYER_SEND_RESULT sr;
        LINK_LAYER_RECEIVE_RESULT rr;
        BocpStatusCode status;

        // 메시지 전송
        BYTE buf[BOCP_MAX_PAYLOAD_SIZE];
        buf[0] = BOCP_MSG_CONTROL_MOTOR_POWER_1;
        buf[1] = (params.fPowerOn) ? 1 : 0;

        m_linkLayer.Send(buf, 2, &sr, m_hShutdownEvent);
        if (BOCP_FAILED(sr.status))
        {
            command.pArgs->status = sr.status;
            command.pArgs->lastWin32Error = sr.lastWin32Error;

            if (sr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 응답 수신
        m_linkLayer.Receive(buf, &rr, m_hShutdownEvent);
        if (BOCP_FAILED(rr.status))
        {
            command.pArgs->status = rr.status;
            command.pArgs->lastWin32Error = rr.lastWin32Error;

            if (rr.status == BOCP_E_CANCELED) *pbCanceled = true;
            return;
        }

        // 응답 해석
        if (IsCommonErrorResponse(buf, rr.ReceivedMessageLength, &status)) // CER인 경우 그 포맷에 맞추어 상태 코드를 읽어서 status를 본다.
        {
            // pair 응답을 전송받은 것이 아니라 got common error response; in fact, something is wrong.
        }
        else
        {
            if (rr.ReceivedMessageLength != 3 || buf[0] != BOCP_MSG_CONTROL_MOTOR_POWER_2)
            {
                command.pArgs->status = BOCP_E_INVALID_RESPONSE;
                command.pArgs->lastWin32Error = 0;
                return;
            }
            status = ntohs(*(u_short *)(buf + 1));
        }

        command.pArgs->status = status;
        command.pArgs->lastWin32Error = 0;
    }
    BOOL BocpHost::IsCommonErrorResponse(LPVOID lpBuffer, DWORD szMessage, BocpStatusCode *pStatus)
    {
        LPBYTE lpData = (LPBYTE)lpBuffer;
        if (lpData[0] != BOCP_MSG_COMMON_ERROR_RESPONSE || szMessage != 4)
            return FALSE;

        *pStatus = ntohs(*(u_short *)(lpData + 1));
        return TRUE;
    }
}
