
#pragma once

#include "BocpDefinitions.h"
#include "LinkLayerDecoder.h"
#include "BocpSerial.h"

namespace BocpDriver
{
    struct LINK_LAYER_RESULT_BASE
    {
        BocpStatusCode status;
        DWORD lastWin32Error; // status가 FAIL인데도 Win32 오류 코드는 ERROR_SUCCESS일수도 있다.
    };

    struct LINK_LAYER_CONNECT_RESULT : public LINK_LAYER_RESULT_BASE
    {

    };
    struct LINK_LAYER_SEND_RESULT : public LINK_LAYER_RESULT_BASE
    {

    };
    struct LINK_LAYER_RECEIVE_RESULT : public LINK_LAYER_RESULT_BASE
    {
        LPVOID Buffer;
        DWORD ReceivedMessageLength;
    };
    struct LINK_LAYER_DISCONNECT_RESULT : public LINK_LAYER_RESULT_BASE
    {

    };

    typedef struct
    {
        BYTE PayloadLength;
        BYTE Flags;
        BYTE SeqNum;
        BYTE Payload[BOCP_MAX_PAYLOAD_SIZE];
    } BOCPPACKET, *LPBOCPPACKET;
    typedef const BOCPPACKET *LPCBOCPPACKET;

    // synchronous link layer
    class BocpHostLinkLayer
    {
    public:
        BocpHostLinkLayer();
        ~BocpHostLinkLayer();

    public:
        void Connect(LPCWSTR lpszDevice, LINK_LAYER_CONNECT_RESULT *pCr, HANDLE hCancellationEvent);
        void Send(LPCVOID lpMessage, size_t szMessage, LINK_LAYER_SEND_RESULT *pSr, HANDLE hCancellationEvent);
        // 수신할 데이터의 크기를 모르므로, lpBuffer는 BOCP_MAX_PAYLOAD_SIZE의 크기가 보장되어야 함
        void Receive(LPVOID lpBuffer, LINK_LAYER_RECEIVE_RESULT *pRr, HANDLE hCancellationEvent);
        void Disconnect(LINK_LAYER_DISCONNECT_RESULT *pDr, HANDLE hCancellationEvent);

        BOOL IsConnected();

    private:
        BocpStatusCode SendSyncPattern(HANDLE hCancellationEvent, DWORD dwTimeout = BOCP_CONNECTION_TIMEOUT_MILLISECONDS);
        BocpStatusCode SendSYN(HANDLE hCancellationEvent);
        BocpStatusCode SendFIN(HANDLE hCancellationEvent);
        BocpStatusCode SendMessageToDevice(LPCVOID lpBuffer, DWORD szMessage, HANDLE hCancellationEvent);
        BocpStatusCode ReceiveFromDeviceCore(LPBOCPPACKET lpReceivedPacket, DWORD dwStartTimestamp, HANDLE hCancellationEvent);

        // 아래 두 함수만 seqNum을 증감함
        BocpStatusCode SendToDevice(LPCVOID lpBuffer, DWORD szPayload, BYTE flags, HANDLE hCancellationEvent,
            DWORD dwTimeout = BOCP_CONNECTION_TIMEOUT_MILLISECONDS);
        BocpStatusCode ResendToDevice(HANDLE hCancellationEvent, DWORD dwTimeout);
        BocpStatusCode ReceiveFromDevice(LPBOCPPACKET lpReceivedPacket, HANDLE hCancellationEvent);

    private:
        BocpSerial m_serial;
        //CSerial m_serial;
        LinkLayerDecoder m_linkLayerDecoder;

        BOCPPACKET m_lastPacketSent; // request 송신 이후 response 수신 직전까지 유효
        BOOL m_fWaitingForResponse;

        // 시퀀스 번호는 
        //   i) 호스트(나)가 패킷을 보내고
        //  ii) 디바이스로부터 동일 시퀀스 번호의 응답을 받으면 하나 증가한다.
        // => 보낼 때는, 그냥 이걸 써서 보낸다.
        //    받을 때는, 같은지 확인해야 한다. (다르면 오류) -> 오류처리는 나중에
        BYTE m_packetSeqNum;

        BYTE m_decoderFeed[BOCP_MAX_STUFFED_PACKET_SIZE];
        size_t m_szDecoderFeed;
    };
}
