// BocpDriverDotNet.h

#pragma once

#include "BocpHost.h"

namespace BocpDriverDotNet
{
    using namespace System;
    using namespace System::Threading::Tasks;

    public enum class BocpOperation : int
    {
        Connect = 0,
        Disconnect = 1,
        QuerySpec = 2,
        QueryOscillationState = 3,
        QuerySetting = 4,
        UpdateSetting = 5,
        StartConstantVelocityMode = 6,
        StartHdmUsingSignal = 7,
        StartHdmUsingDerivative = 8,
        StopOscillation = 9,
        StopHostDriven = 10,
        StartSinusoidalDrive = 11,
        FineControl = 12,
        ControlMotorPower = 13
    };
    public enum class BocpStatusCode : unsigned short
    {
        Success = 0x0000,
        Unknown = 0xFFFF,
        InvalidRequest = 0x8000,
        Unsupported = 0x8001,
        OutOfRange = 0x8002,
        InvalidParameter = 0x8003,
        InvalidOperation = 0x8004,
        Timeout = 0x8005,
        Protocol = 0x8006,
        IoError = 0x8007,
        Canceled = 0x8008,
        InvalidResponse = 0x8009
    };
    public ref class DeviceSpec
    {
    public:
        property String^ Manufacturer
        {
            String^ get() { return m_manufacturer; }
            void set(String^ value) { m_manufacturer = value; }
        }
        property String^ ModelName
        {
            String^ get() { return m_modelName; }
            void set(String^ value) { m_modelName = value; }
        }
        property System::UInt16 Capabilities
        {
            System::UInt16 get() { return m_capabilities; }
            void set(System::UInt16 value) { m_pulsePerRev = value; }
        }
        property System::UInt32 PulsePerRev
        {
            System::UInt32 get() { return m_pulsePerRev; }
            void set(System::UInt32 value) { m_pulsePerRev = value; }
        }
        property System::UInt32 DistancePerRev
        {
            System::UInt32 get() { return m_distancePerRev; }
            void set(System::UInt32 value) { m_distancePerRev = value; }
        }
    private:
        String^ m_manufacturer;
        String^ m_modelName;
        System::UInt16 m_capabilities;
        System::UInt32 m_pulsePerRev;
        System::UInt32 m_distancePerRev;
    };
    public enum class OscillationMode
    {
        Stopped = 0,
        ConstantVelocity = 1,
        HostDriven = 2,
        CalibrationInProgress = 3,
        PredefinedWaveform = 4
    };
    public ref class DeviceSetting abstract
    {
    internal:
        DeviceSetting() {} // 현재 어셈블리 외부에서 이 클래스를 상속하는 것을 방지

    public:
        virtual property System::Byte SettingCode
        {
            System::Byte get() abstract;
        }
        virtual property System::Int32 Length
        {
            System::Int32 get() abstract;
        }
        virtual array<System::Byte>^ Serialize() = 0;
        virtual bool Deserialize(array<System::Byte>^ data) = 0;
    };
    public ref class PlusDirectionSetting sealed : public DeviceSetting
    {
    public:
        literal System::Byte DirectionA = 0x00;
        literal System::Byte DirectionB = 0x01;
        virtual property System::Byte SettingCode
        {
            System::Byte get() override { return 0x00; }
        }
        virtual property System::Int32 Length
        {
            System::Int32 get() override { return 1; }
        }
        virtual array<System::Byte>^ Serialize() override
        {
            array<System::Byte>^ buf = gcnew array<System::Byte>(1);
            buf[0] = m_plusDirection;
            return buf;
        }
        virtual bool Deserialize(array<System::Byte>^ data) override
        {
            if (data == nullptr || data->Length != Length)
                return false;

            System::Byte plusDirection = data[0];
            if (plusDirection != DirectionA && plusDirection != DirectionB)
                return false;

            m_plusDirection = plusDirection;
            return true;
        }

        property System::Byte PlusDirection
        {
            System::Byte get() { return m_plusDirection; }
            void set(System::Byte value)
            {
                if (value != DirectionA && value != DirectionB)
                    throw gcnew ArgumentException("PlusDirection");
                m_plusDirection = value;
            }
        }
    private:
        System::Byte m_plusDirection = DirectionA;
    };
    /*public ref class BocpAsyncEventArgs
    {
    public:
        property BocpOperation LastOperation
        {
            BocpOperation get() { return m_lastOperation; }
            void set(BocpOperation value) { m_lastOperation = value; }
        }
        property BocpStatusCode Result
        {
            BocpStatusCode get() { return m_errorCode; }
            void set(BocpStatusCode value) { m_errorCode = value; }
        }
        property DeviceSpec^ DeviceSpec
        {
            BocpDriverDotNet::DeviceSpec^ get() { return m_deviceSpec; }
            void set(BocpDriverDotNet::DeviceSpec^ value) { m_deviceSpec = value; }
        }
        property OscillationMode OscillationMode
        {
            BocpDriverDotNet::OscillationMode get() { return m_currentMode; }
            void set(BocpDriverDotNet::OscillationMode value) { m_currentMode = value; }
        }
        property DeviceSetting^ Setting
        {
            DeviceSetting^ get() { return m_setting; }
            void set(DeviceSetting^ value) { m_setting = value; }
        }

    private:
        BocpOperation m_lastOperation;
        BocpStatusCode m_errorCode;
        BocpDriverDotNet::DeviceSpec^ m_deviceSpec;
        BocpDriverDotNet::OscillationMode m_currentMode;
        DeviceSetting^ m_setting;
    };
    public delegate void BocpCallback(Object^ sender, BocpAsyncEventArgs^ e);*/
    public ref class BocpOperationResult
    {
    public:
        property BocpOperation Operation
        {
            BocpOperation get() { return m_lastOperation; }
            void set(BocpOperation value) { m_lastOperation = value; }
        }
        property BocpStatusCode Status
        {
            BocpStatusCode get() { return m_errorCode; }
            void set(BocpStatusCode value) { m_errorCode = value; }
        }
    private:
        BocpOperation m_lastOperation;
        BocpStatusCode m_errorCode;
    };

    public ref class BocpHostDotNet
    {
    public:
        BocpHostDotNet();
        ~BocpHostDotNet();

    public:
        /*
        1) 연결 수립 (시리얼 포트 열기 + BOCP 핸드셰이크)
        2) 명령 전달 (HDM은 "시간 -> signal값" 반환하는 함수를 전달받고, 내부적으로 기하학적 변환을 수행하여 데이터생성하도록 한다)
        3) 연결 종료

        모든 함수는 비동기적으로 수행되도록 하자. (overlapped I/O API 이용)
        - Arduino에서는 Serial library가 polling을 수행했지만, 여기서는 overlapped I/O API에 의해 event-based 구동이다.

        void BocpCallback(object sender, BocpAsyncEventArgs e);
        BocpAsyncEventArgs:
        BocpAsyncOperation LastOperation
        BocpHostError ErrorCode (error code) <-- timeout, device doesn't support X, etc
        DeviceSpec DeviceSpec
        DeviceOscillationState OscillationState
        DeviceSetting Setting (<-- DeviceSetting is an abstract class; it is not inheritable outside the assembly)
        (<-- DeviceSetting은 setting query에 응답 시에 설정됨)

        BocpHostErrorCode ConnectAsync(String portName, BocpCallback callback);
        void StartConstantVelocityMode(long micrometerPerSecond);
        void StartHdmUsingSignal(Func<long, long> signalFunction, long period);
        void StartHdmUsingDerivative(Func<long, long> signalDerivative, long period);
        void StopOscillation();

        void QuerySpecAsync(BocpCallback callback);
        void QueryOscillationStateAsync(BocpCallback callback);
        void QuerySettingAsync(SettingCode settingCode, BocpCallback callback);
        void UpdateSettingAsynnc(DeviceSetting newSetting, BocpCallback callback);

        String CurrentPortName { get; }

        */
        Task<BocpOperationResult ^> ^ConnectAsync(String^ portName);
        Task<BocpOperationResult ^> ^DisconnectAsync();
        
        Task<BocpOperationResult ^> ^ControlMotorPowerAsync(bool fPowerOn);

        Task<BocpOperationResult ^> ^FineControlAsync(int32_t steps);
        Task<BocpOperationResult ^> ^StartLinearConstantVelocityModeAsync(double micrometersPerSecond);
        Task<BocpOperationResult ^> ^StartRotationalConstantVelocityModeAsync(double microradiansPerSecond);

        Task<BocpOperationResult ^> ^StartLinearSinusoidalDriveAsync(double amplitudeInMicrometers, double angularFrequencyInRadPerSec, double phaseConstantInRad);
        Task<BocpOperationResult ^> ^StartRotationalSinusoidalDriveAsync(double amplitudeInMicroradians, double angularFrequencyInRadPerSec, double phaseConstantInRad);
        Task<BocpOperationResult ^> ^StartHdmUsingSignalAsync(Func<long, long>^ signalFunction, long periodInMicroseconds); // Signal은 [um]의 단위를 가짐
        Task<BocpOperationResult ^> ^StartHdmUsingDerivativeAsync(Func<long, long>^ signalDerivative, long periodInMicroseconds); // Derivative는 [um/s]의 단위를 가짐
        Task<BocpOperationResult ^> ^StopOscillationAsync();

        Task<BocpOperationResult ^> ^QuerySpecAsync();
        Task<BocpOperationResult ^> ^QueryOscillationStateAsync();
        Task<BocpOperationResult ^> ^QuerySettingAsync(System::Byte settingCode);
        Task<BocpOperationResult ^> ^UpdateSettingAsync(DeviceSetting^ newSetting);

    private:
        delegate void BocpCallbackPrototype(IntPtr pArgsPtr);
        static void BocpCallback(IntPtr pArgsPtr);

        BocpCallbackPrototype ^m_bocpCallbackGCRoot;
        BocpDriver::BOCP_CALLBACK m_bocpCallbackPtr;

    private:
        System::Object ^m_syncObj;
        BocpDriver::BocpHost *m_pNativeImpl;
    };
}
