﻿
#pragma once

#include "BocpDefinitions.h"

class LinkLayerEncoder
{
public:
    LinkLayerEncoder();
    virtual ~LinkLayerEncoder();

public:
    // payload_len > 0인 경우 payload는 유효한 포인터를 가리켜야 함
    // 출력 버퍼 (out)에는 최소한 BOCP_MAX_STUFFED_PACKET_SIZE의 용량이 공급되어야 함
    // 버퍼 크기만 충분하면 무조건 성공함
    static bool GenerateSavePacket(const void *payload, size_t payload_len, uint8_t flags, uint8_t seqNum, void *out, size_t *written);

//public:
//    // 전송 시작. 만약 현재 상태가 SS_STATE_READY가 아닌 경우, 전송에 실패함(false 반환).
//    // 이때는 Reset()을 호출해야 함
//    // * len: payload의 크기
//    bool StartSending(const void *buf, int len, uint8_t flags, uint8_t seqNum, bool sendSyncSignal);
//    bool IsSendComplete();
//    void Reset();
//
//private:
//    uint8_t m_state;
//
//    // Sync signal ('00') 관련
//    bool m_bSendSyncSignal;
//    int m_syncSignalSentBytes;
//
//    // Packet (framed payload) 관련
//    uint8_t m_packet[BOCP_MAX_PACKET_SIZE];
//    int m_packetLength;
//
//    COBSEncoder m_cobsEncoder;
};
