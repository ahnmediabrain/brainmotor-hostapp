
#include "stdafx.h"
#include "ScopedEventHandle.h"

namespace BocpDriver
{
    ScopedEventHandle::ScopedEventHandle(bool bManualResetEvent, bool bInitialState)
    {
        m_hEvent = ::CreateEvent(NULL, bManualResetEvent, bInitialState, NULL);
    }

    ScopedEventHandle::~ScopedEventHandle()
    {
        ::CloseHandle(m_hEvent);
    }
    HANDLE ScopedEventHandle::Handle()
    {
        return m_hEvent;
    }
}
