
#pragma once

namespace BocpDriver
{
    // a RAII event handle
    class ScopedEventHandle
    {
    public:
        ScopedEventHandle(bool bManualResetEvent, bool bInitialState);
        ScopedEventHandle(const ScopedEventHandle &) = delete;
        ScopedEventHandle& operator=(const ScopedEventHandle &) = delete;
        ~ScopedEventHandle();

    public:
        HANDLE Handle();

    private:
        HANDLE m_hEvent;
    };
}
