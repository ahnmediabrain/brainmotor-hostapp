
#include "stdafx.h"
#include "BocpHost.h"
#include "BocpHostDotNet.h"

#include <vcclr.h>
#include <msclr/gcroot.h>

using namespace System;
using namespace System::Threading::Tasks;
using namespace System::Runtime::InteropServices;

namespace BocpDriverDotNet
{
    enum BocpConnectionState : int
    {
        NoConnection,
        SendingHandshakeHello,
        WaitingHandshakeResponse,
        Connected,
        WaitingTimeout
    };
    enum HdmState : int
    {
        NoHdm,
        HdmPrepare1Sent,
        HdmPrepare2Received,
        HdmInitialBuffering,
        HdmOscDataTransmission
    };

    struct CallbackData
    {
        BocpDriver::BOCP_OPERATION_ARGS Args;
        LPVOID TcsGCHandle; // GCHandle to a TaskCompletionSource<int>
    };

    /*
    시리얼 통신 방식:
        1) ConnectAsync 호출 시, 연결 시작해야 함
            a. COM 포트 열기
            b. Handshake Hello 전송 (SerialModule)
            c. 응답 대기.
            d. Timeout 이전에 받으면 연결 수립으로 간주
            e. Timeout되면 연결 리셋으로 간주
        2) 모든 명령은 Connected 상태에서만 수행 가능함 (다른 상황에서는 Exception 발생)
        3) 각 명령에 대해, 
        4) HDM 명령은 여러 번에 걸쳐서 수행되어야 함 -> HDM 수행 중일 때도 
    동기화 문제 피하기:
        ReadFileEx는 WriteFileEx가 끝났다는 callback 받았을 때 호출한다.
        WriteFileEx는 ReadFileEx가 끝났다는 callback 받았을 때, 메시지를 해석한 후 적절한 응답을 담아 호출한다.
    */

    BocpHostDotNet::BocpHostDotNet()
    {
        m_syncObj = gcnew System::Object();
        m_pNativeImpl = new BocpDriver::BocpHost;

        BocpCallbackPrototype ^callback = gcnew BocpCallbackPrototype(BocpHostDotNet::BocpCallback);
        m_bocpCallbackGCRoot = callback;
        m_bocpCallbackPtr = (BocpDriver::BOCP_CALLBACK)Marshal::GetFunctionPointerForDelegate(callback).ToPointer();
    }
    BocpHostDotNet::~BocpHostDotNet()
    {
        this->DisconnectAsync()->Wait();
        delete m_pNativeImpl;
    }
    Task<BocpOperationResult ^> ^BocpHostDotNet::ConnectAsync(String^ portName)
    {
        try
        {
            System::Threading::Monitor::Enter(this->m_syncObj);
            if (portName->Length > MAX_PATH)
                throw gcnew System::ArgumentException("portName should be shorter than MAX_PATH (or equal)");

            // this will be signaled in the callback
            TaskCompletionSource<BocpOperationResult ^> ^tcs = gcnew TaskCompletionSource<BocpOperationResult ^>();
            pin_ptr<const wchar_t> szPortName = PtrToStringChars(portName);

            CallbackData *callbackData = new CallbackData;
            GCHandle gch = GCHandle::Alloc(tcs);
            callbackData->Args.lpUser = callbackData; // 콜백에는 BOCP_OPERATION_ARGS *만 넘어오므로, reference를 저장해 놓아야 한다.
            callbackData->Args.callback = m_bocpCallbackPtr;
            callbackData->TcsGCHandle = GCHandle::ToIntPtr(gch).ToPointer();

            m_pNativeImpl->ConnectAsync(szPortName, &callbackData->Args);

            return tcs->Task;
        }
        finally
        {
            System::Threading::Monitor::Exit(this->m_syncObj);
        }
    }
    System::Threading::Tasks::Task<BocpOperationResult ^> ^BocpHostDotNet::DisconnectAsync()
    {
        try
        {
            System::Threading::Monitor::Enter(this->m_syncObj);

            // this will be signaled in the callback
            TaskCompletionSource<BocpOperationResult ^> ^tcs = gcnew TaskCompletionSource<BocpOperationResult ^>();

            CallbackData *callbackData = new CallbackData;
            GCHandle gch = GCHandle::Alloc(tcs);
            callbackData->Args.lpUser = callbackData; // 콜백에는 BOCP_OPERATION_ARGS *만 넘어오므로, reference를 저장해 놓아야 한다.
            callbackData->Args.callback = m_bocpCallbackPtr;
            callbackData->TcsGCHandle = GCHandle::ToIntPtr(gch).ToPointer();

            m_pNativeImpl->DisconnectAsync(&callbackData->Args);

            return tcs->Task;
        }
        finally
        {
            System::Threading::Monitor::Exit(this->m_syncObj);
        }
    }
    Task<BocpOperationResult ^> ^BocpDriverDotNet::BocpHostDotNet::ControlMotorPowerAsync(bool fPowerOn)
    {
        try
        {
            System::Threading::Monitor::Enter(this->m_syncObj);

            // this will be signaled in the callback
            TaskCompletionSource<BocpOperationResult ^> ^tcs = gcnew TaskCompletionSource<BocpOperationResult ^>();

            CallbackData *callbackData = new CallbackData;
            GCHandle gch = GCHandle::Alloc(tcs);
            callbackData->Args.lpUser = callbackData; // 콜백에는 BOCP_OPERATION_ARGS *만 넘어오므로, reference를 저장해 놓아야 한다.
            callbackData->Args.callback = m_bocpCallbackPtr;
            callbackData->TcsGCHandle = GCHandle::ToIntPtr(gch).ToPointer();

            m_pNativeImpl->ControlMotorPowerAsync(fPowerOn, &callbackData->Args);

            return tcs->Task;
        }
        finally
        {
            System::Threading::Monitor::Exit(this->m_syncObj);
        }
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::FineControlAsync(int32_t steps)
    {
        try
        {
            System::Threading::Monitor::Enter(this->m_syncObj);

            // this will be signaled in the callback
            TaskCompletionSource<BocpOperationResult ^> ^tcs = gcnew TaskCompletionSource<BocpOperationResult ^>();

            CallbackData *callbackData = new CallbackData;
            GCHandle gch = GCHandle::Alloc(tcs);
            callbackData->Args.lpUser = callbackData; // 콜백에는 BOCP_OPERATION_ARGS *만 넘어오므로, reference를 저장해 놓아야 한다.
            callbackData->Args.callback = m_bocpCallbackPtr;
            callbackData->TcsGCHandle = GCHandle::ToIntPtr(gch).ToPointer();

            m_pNativeImpl->FineControlAsync(steps, &callbackData->Args);

            return tcs->Task;
        }
        finally
        {
            System::Threading::Monitor::Exit(this->m_syncObj);
        }
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::StartLinearConstantVelocityModeAsync(double micrometersPerSecond)
    {
        try
        {
            System::Threading::Monitor::Enter(this->m_syncObj);

            // this will be signaled in the callback
            TaskCompletionSource<BocpOperationResult ^> ^tcs = gcnew TaskCompletionSource<BocpOperationResult ^>();

            CallbackData *callbackData = new CallbackData;
            GCHandle gch = GCHandle::Alloc(tcs);
            callbackData->Args.lpUser = callbackData; // 콜백에는 BOCP_OPERATION_ARGS *만 넘어오므로, reference를 저장해 놓아야 한다.
            callbackData->Args.callback = m_bocpCallbackPtr;
            callbackData->TcsGCHandle = GCHandle::ToIntPtr(gch).ToPointer();

            m_pNativeImpl->StartLinearConstantVelocityModeAsync(micrometersPerSecond, &callbackData->Args);

            return tcs->Task;
        }
        finally
        {
            System::Threading::Monitor::Exit(this->m_syncObj);
        }
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::StartRotationalConstantVelocityModeAsync(double microradiansPerSecond)
    {
        try
        {
            System::Threading::Monitor::Enter(this->m_syncObj);

            // this will be signaled in the callback
            TaskCompletionSource<BocpOperationResult ^> ^tcs = gcnew TaskCompletionSource<BocpOperationResult ^>();

            CallbackData *callbackData = new CallbackData;
            GCHandle gch = GCHandle::Alloc(tcs);
            callbackData->Args.lpUser = callbackData; // 콜백에는 BOCP_OPERATION_ARGS *만 넘어오므로, reference를 저장해 놓아야 한다.
            callbackData->Args.callback = m_bocpCallbackPtr;
            callbackData->TcsGCHandle = GCHandle::ToIntPtr(gch).ToPointer();

            m_pNativeImpl->StartRotationalConstantVelocityModeAsync(microradiansPerSecond, &callbackData->Args);

            return tcs->Task;
        }
        finally
        {
            System::Threading::Monitor::Exit(this->m_syncObj);
        }
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::StartLinearSinusoidalDriveAsync(double amplitudeInMicrometers, double angularFrequencyInRadPerSec, double phaseConstantInRad)
    {
        try
        {
            System::Threading::Monitor::Enter(this->m_syncObj);

            // this will be signaled in the callback
            TaskCompletionSource<BocpOperationResult ^> ^tcs = gcnew TaskCompletionSource<BocpOperationResult ^>();

            CallbackData *callbackData = new CallbackData;
            GCHandle gch = GCHandle::Alloc(tcs);
            callbackData->Args.lpUser = callbackData; // 콜백에는 BOCP_OPERATION_ARGS *만 넘어오므로, reference를 저장해 놓아야 한다.
            callbackData->Args.callback = m_bocpCallbackPtr;
            callbackData->TcsGCHandle = GCHandle::ToIntPtr(gch).ToPointer();

            m_pNativeImpl->StartLinearSinusoidalDriveAsync(amplitudeInMicrometers, angularFrequencyInRadPerSec, phaseConstantInRad, &callbackData->Args);

            return tcs->Task;
        }
        finally
        {
            System::Threading::Monitor::Exit(this->m_syncObj);
        }
    }
    Task<BocpOperationResult ^> ^BocpDriverDotNet::BocpHostDotNet::StartRotationalSinusoidalDriveAsync(double amplitudeInMicroradians, double angularFrequencyInRadPerSec, double phaseConstantInRad)
    {
        try
        {
            System::Threading::Monitor::Enter(this->m_syncObj);

            // this will be signaled in the callback
            TaskCompletionSource<BocpOperationResult ^> ^tcs = gcnew TaskCompletionSource<BocpOperationResult ^>();

            CallbackData *callbackData = new CallbackData;
            GCHandle gch = GCHandle::Alloc(tcs);
            callbackData->Args.lpUser = callbackData; // 콜백에는 BOCP_OPERATION_ARGS *만 넘어오므로, reference를 저장해 놓아야 한다.
            callbackData->Args.callback = m_bocpCallbackPtr;
            callbackData->TcsGCHandle = GCHandle::ToIntPtr(gch).ToPointer();

            m_pNativeImpl->StartRotationalSinusoidalDriveAsync(amplitudeInMicroradians, angularFrequencyInRadPerSec, phaseConstantInRad, &callbackData->Args);

            return tcs->Task;
        }
        finally
        {
            System::Threading::Monitor::Exit(this->m_syncObj);
        }
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::StartHdmUsingSignalAsync(Func<long, long>^ signalFunction, long periodInMicroseconds)
    {
        throw gcnew System::NotImplementedException();
        // TODO: 여기에 반환 구문을 삽입합니다.
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::StartHdmUsingDerivativeAsync(Func<long, long>^ signalDerivative, long periodInMicroseconds)
    {
        throw gcnew System::NotImplementedException();
        // TODO: 여기에 반환 구문을 삽입합니다.
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::StopOscillationAsync()
    {
        try
        {
            System::Threading::Monitor::Enter(this->m_syncObj);

            // this will be signaled in the callback
            TaskCompletionSource<BocpOperationResult ^> ^tcs = gcnew TaskCompletionSource<BocpOperationResult ^>();

            CallbackData *callbackData = new CallbackData;
            GCHandle gch = GCHandle::Alloc(tcs);
            callbackData->Args.lpUser = callbackData; // 콜백에는 BOCP_OPERATION_ARGS *만 넘어오므로, reference를 저장해 놓아야 한다.
            callbackData->Args.callback = m_bocpCallbackPtr;
            callbackData->TcsGCHandle = GCHandle::ToIntPtr(gch).ToPointer();

            m_pNativeImpl->StopOscillationAsync(&callbackData->Args);

            return tcs->Task;
        }
        finally
        {
            System::Threading::Monitor::Exit(this->m_syncObj);
        }
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::QuerySpecAsync()
    {
        throw gcnew System::NotImplementedException();
        // TODO: 여기에 반환 구문을 삽입합니다.
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::QueryOscillationStateAsync()
    {
        throw gcnew System::NotImplementedException();
        // TODO: 여기에 반환 구문을 삽입합니다.
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::QuerySettingAsync(System::Byte settingCode)
    {
        throw gcnew System::NotImplementedException();
        // TODO: 여기에 반환 구문을 삽입합니다.
    }
    Task<BocpOperationResult^>^ BocpDriverDotNet::BocpHostDotNet::UpdateSettingAsync(DeviceSetting ^ newSetting)
    {
        throw gcnew System::NotImplementedException();
        // TODO: 여기에 반환 구문을 삽입합니다.
    }
    void BocpDriverDotNet::BocpHostDotNet::BocpCallback(IntPtr pArgsPtr)
    {
        const BocpDriver::BOCP_OPERATION_ARGS_BASE *pArgs = (const BocpDriver::BOCP_OPERATION_ARGS_BASE *)pArgsPtr.ToPointer();
        CallbackData *pCallbackData = (CallbackData *)pArgs->lpUser;
        GCHandle gch = GCHandle::FromIntPtr(IntPtr(pCallbackData->TcsGCHandle));
        TaskCompletionSource<BocpOperationResult ^> ^tcs = (TaskCompletionSource<BocpOperationResult ^> ^)gch.Target;

        // 네이티브 구조체를 통해 전달된 데이터를 CLR 타입으로 변환
        BocpOperationResult ^operationResult = gcnew BocpOperationResult();
        operationResult->Operation = (BocpOperation)pArgs->operation;
        operationResult->Status = (BocpStatusCode)pArgs->status;

        tcs->SetResult(operationResult);
        gch.Free();
        
        delete pCallbackData;
    }
}
